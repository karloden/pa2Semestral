#ifndef FALLBUILD_PARAMETERS_H
#define FALLBUILD_PARAMETERS_H

#include <fstream>
#include <cstdint>
#include <vector>
#include <iostream>
#include <map>
#include <atomic>
#include "exceptions.h"

extern const std::string g_ConfigDefault;
extern const std::string g_Config;
extern const int g_BuildingsCount;


/**
 * \class Singleton class used to get and set game parameters of all purposes
 */
class Parameters {
public:
    /**
     * \brief Sets config files paths
     */
    Parameters() : m_AllInit(false), m_LoadedGame(false) {}

    /**
     * \parblock Prohibits singleton instance being copied or moved
     */
    Parameters(const Parameters &) = delete;

    Parameters &operator=(const Parameters &) = delete;

    Parameters(const Parameters &&) = delete;

    Parameters &operator=(const Parameters &&) = delete;
    /**
    * \endparblock
    */

    /**
     * \brief Cleans memory, probably allocated by singleton instance.
     */
    ~Parameters();

    /**
     * @return resolution X.
     */
    int32_t resX() const { return m_ResX; }

    /**
     * @return resolution Y.
     */
    int32_t resY() const { return m_ResY; }

    /**
     * @return Fullscreen flag.
     */
    bool fullscreen() const { return m_Fullscreen; }

    /**
     * @return Map width.
     */
    int32_t mapW() const { return m_LoadedGame ? m_MapWSave : m_MapW; }

    /**
     * @return Map height.
     */
    int32_t mapH() const { return m_LoadedGame ? m_MapHSave : m_MapH; }

    /**
     * @return Camera sensitivity.
     */
    int32_t cameraSens() { return m_Sens; }

    /**
    * @return size of the smallest sprite texture in pixels
    */
    uint32_t textureSizeSmall() const { return m_TextureSmall; }

    /**
    * @return size of medium sprite texture in pixels
    */
    uint32_t textureSizeMedium() const { return m_TextureMedium; }

    /**
    * @return size of the biggest sprite texture in pixels
    */
    uint32_t textureSizeBig() const { return m_TextureBig; }

    /**
    * \brief Sets sizes of textures in pixels, according to red sprites map
     * @param small
     * @param medium
     * @param big
     */
    void setTexturesSizes(uint32_t small, uint32_t medium, uint32_t big);

    /**
     * \brief Parses and sets graphics parameters from files (default and then user's)
     * \throws ConfigFileFormatException if one of 3 category (game, buildings , graphics, was not found)
     */
    void readConfig();

    /**
     * @return true if all configuration parameters was initialized correctly.
     */
    bool allParametersInitialized() const { return m_AllInit; }

    /**
     * @return Singleton instance
     */
    static Parameters &getInstance();

    /**
     * @return Ground tile used more
     */
    uint32_t getEnvironmentFirst() const;

    /**
     * @return Ground tile used less
     */
    uint32_t getEnvironmentSecond() const;

    /**
     * @return Money on the beginning, strongly affects game difficulty
     */
    int32_t getInitMoney() const { return m_InitMoney; }

    /**
    * \brief Struct used to save building parameters values.
    */
    struct BuildingParameters {
        /**
         * constructor used to initialize instance with parameters from file
         * @param moneyPs money per second by building of instance type
         * @param happyPs happiness per second by building of instance type
         * @param crimePs crime per second by
         * @param givePeople building of instance type gives people
         * @param cost cost of building
         */
        BuildingParameters(int32_t moneyPs, int32_t happyPs, int32_t crimePs, int32_t givePeople, int32_t cost)
                : m_MoneyPs(moneyPs), m_HappyPs(happyPs), m_CrimePs(crimePs), m_GivePeople(givePeople), m_Cost(cost) {}

        /**
         * @param serialized pointer to array, used to parse parameters from save file
         */
        BuildingParameters(const void* serialized);

        /**
         * @return serialized pointer to array, used to insert in a save file
         */
        void* serialize() const;

        /**
         * @param serialized pointer to data array
         * @param ind index of integer being read
         * @return integer parsed from binary serialized array
         */
        static int32_t parseInt(const void* serialized, int32_t ind);

        ~BuildingParameters() {}

        BuildingParameters(const BuildingParameters &) = delete;

        BuildingParameters &operator=(const BuildingParameters &) = delete;

        const int32_t m_MoneyPs, m_HappyPs, m_CrimePs, m_GivePeople, m_Cost;
    };

    /**
     * \brief used to determine how much every building produce/costs in-game state
     * @return reference to vector of building parameters
     */
    const std::vector<BuildingParameters*> &getBuildingParameters() {
        return m_LoadedGame ? m_BuildingParamsSave : m_BuildingParameters;
    }

    /**
      * @return serialized pointer to array, used to insert in a save file
     */
    void* serialize();

    /**
     * \brief deserializer, used to load old game parameters, in order to prohibit cheating
      * @param serialized binary array of parameters
      */
    void readGameAndBuildingsParamsFromSave(const void* serialized);

    /**
     *
     * @param enabled switches state of loaded or new game
     */
    void switchLoadedParams(bool enabled) { m_LoadedGame = enabled; }

private:
    /**
     * \brief Parses changeable graphics parameters from vector
     * \throws ConfigFileFormatException if one of parameters was not found
     * @param parameters vector with parameters
     */
    void parseGraphics(const std::vector<std::string> &parameters);

    /**
     * \brief Parses changeable game parameters from vector
     * \throws ConfigFileFormatException if one of parameters was not found
     * @param parameters vector with parameters
     */
    void parseGame(const std::vector<std::string> &parameters);

    /**
     * \brief Parses changeable buildings parameters from vector
     * \throws ConfigFileFormatException if one of parameters was not found
     * @param buildings parameters vector
      */
    void parseBuildings(const std::vector<std::string> &buildings);

    /**
     * \brief Checks if user's file exists and copying it from default config if not.
     * \throws FileOpenException if nor of them exist
     * @param is input stream where file might to be opened
     * @param paramPath path to file
     */
    void checkAndRecoverConfig(std::ifstream &paramStream, const std::string &paramPath) const;

    /**
    * \brief Parses all parameters from the file, using specified functions
    * For consistence of program work this function is called twice to get default configs from file
    * if configs in user's .cfg file doesn't exist.
     * \throws ConfigFileFormatException if none of category or line in one of config files was found
    * @param paramPath is a path to config file.
    */
    void fillMap(const std::string &paramPath, std::map<std::string, std::vector<std::string>> &parameters);

    std::vector<BuildingParameters*> m_BuildingParameters;
    std::vector<BuildingParameters*> m_BuildingParamsSave;
    uint32_t m_ResX, m_ResY;
    uint32_t m_MapW, m_MapH;
    uint32_t m_MapWSave, m_MapHSave;
    uint32_t m_Sens;
    uint32_t m_Env;
    uint32_t m_EnvSave;
    uint32_t m_TextureSmall, m_TextureMedium, m_TextureBig;
    int32_t m_InitMoney;
    bool m_AllInit;
    bool m_Fullscreen;
    bool m_LoadedGame;
};


#endif //FALLBUILD_PARAMETERS_H
