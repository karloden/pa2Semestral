#ifndef FALLBUILD_MAIN_LOOP_H
#define FALLBUILD_MAIN_LOOP_H

#include <atomic>
#include <vector>
#include "game_logic.h"
#include "display.h"

extern const std::string g_WindowName;
extern const std::string g_Save;

//Predefining StateMachine class in order to have it in friends of MainLoop
class StateMachine;

class MainLoop {
public:
    friend StateMachine;

    /**
     * \brief default constructor initializes instance, that will provide primary functionality of game
     */
    MainLoop();

    /**
     * \brief destructor deallocates memory, that MainLoop instance could allocate
     */
    ~MainLoop();

    /**
     * \brief initializes game main menu
     */
    void initializeMenu();

    /**
     * \brief indicates when application was closed
     * @return true, if user's input was considered as exit command
     */
    bool isClosed() { return m_IsClosed; }


private:
    /**
     * @param layer, from which redrawing should be started.
     */
    void reDraw(int8_t layer) { m_Display.drawSceneObjects(layer); }

    static const int8_t LayerBackground = 0;
    static const int8_t LayerObjects = 1;
    static const int8_t LayerInterface = 2;
    static const int8_t LayersEnd = 3;

    /**
     * @param object pointer to object, that should be deleted
     * @param type represents which type of object is passed to being deleted
     */
    void deleteObject(Drawable* object, ObjectType type);

    /**
     * \brief initializes new game from the very beginning
     */
    void initializeNewGame();

    /**
     * \brief serializes game state and current configurations in order to reload it afterwards
     */
    void gameSave();

    /**
     * \brief loads game state from disk, parsing serialized binary
     * @return true, if load succeeded
     */
    bool loadGame();

    /**
     * \brief opens game menu (in-game menu, containing game save and returning to main menu)
     * Must be called only if build menu is closed.
     * @param enable true if menu will be enabled, false if disabled
     */
    void gameMenu(bool enable) { m_Display.gameMenu(enable); }

    /**
     * \brief opens build menu (in-game menu, containing game save and returning to main menu)
     * * Must be called only if game menu is closed.
     * @param enable true if menu will be enabled, false if disabled
     */
    void buildMenu(bool enable) { m_Display.buildMenu(enable); }

    /**
     * \brief called when mouse was moved to an exact position
     * @param x axis X coordinate
     * @param y axis Y coordinate
     */
    void mouseMovedTo(int32_t x, int32_t y) { m_Display.hoverObject(x, y); }

    /**
     * \brief method is used to make all staff connected with new building creation (init, move, place, release)
     * @param semantics accepts struct instance, with all information mandatory to realize what action ought to be done
     */
    void resolveHolding(const ObjHoldingSemantics &semantics);

    /**
     * \brief changes relative drawing vars, that creates the illusion of changing the position of camera
     * @param direction SDL api keycode enum, that helps to realize direction
     */
    void cameraMove(const SDL_Keycode &direction);

    /**
     * \brief finds which one object was clicked by user
     * @param x axis X coordinate
     * @param y axis Y coordinate
     * @param layer writes a layer here, where clicked object was found
     * @return pointer to object clicked
     */
    Drawable* findOutObjectClicked(int32_t x, int32_t y, int8_t &layer);

    /**
     * \brief displays message with 1 button with callback
     * @param message this will be displayed as a main message
     * @param callback for button
     * @param button string, that will be printed on button
     * @return callback of button was pressed
     */
    CallbackState displayMessageBox(const std::string &message, CallbackState callback, const std::string &button);

    /**
     * \brief displays message with 2 buttons, each one of them has own callback
     * @param message this will be displayed as a main message
     * @param callback1 callback for first button
     * @param button1 string, that will be printed on first button
     * @param callback2 callback for second button
     * @param button2 string, that will be printed on second button
     * @return callback of button was pressed
     */
    CallbackState displayMessageBox(const std::string &message, CallbackState callback1, const std::string &button1,
                                    CallbackState callback2, const std::string &button2);

    Display m_Display;
    std::atomic_bool m_GameOver;
    bool m_IsClosed;
    ObjHoldingSemantics* m_ObjHolding;
    GameLogic* m_GameLogic;
};

#endif //FALLBUILD_MAIN_LOOP_H
