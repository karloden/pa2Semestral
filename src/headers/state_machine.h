#ifndef FALLBUILD_STATE_MACHINE_H
#define FALLBUILD_STATE_MACHINE_H

#include "display.h"
#include "misc.h"
#include "main_loop.h"

/**
 * \brief class used to get user's input and realise next action depending on it
 */
class StateMachine {
public:
    /**
     * \brief constructor used to initialize all variables and build an instance
     * @param injectedMainLoop used to call methods from there, depending on user's input
     */
    StateMachine(MainLoop* injectedMainLoop);

    /**
     * \brief destructor used to clean memory from allocated SDL_Event instance
     */
    ~StateMachine() { delete m_Event; }

    /**
     * \brief gets event from OS's event front using SDL api, and realises which kind of event is it.
     */
    void pollEvent();

private:
    void newInit();

    /**
     * \brief realises which exactly action ought to be done according to input of mouse move
     * @param event mouse move event
     */
    void mouseMoveEvent(const SDL_Event* event);

    /**
     * \brief realises which exactly action ought to be done according to input of keyboard key down
     * @param event keyboard input event
     */
    void keyboardEvent(const SDL_Event* event);

    /**
     * \brief realises which exactly action ought to be done according to input of mouse key down
     * @param event mouse button click event
     */
    void mouseButtonEvent(const SDL_Event* event);

    /**
     * \brief takes one of values of states and callbacks enumerator class and resolves action linked with it
     * @param callback value of enumerator state class
     */
    void resolveCallbackState(const CallbackState &callback);

    /**
     * \brief resolves choosing of building and initiates building holding (chosen from menu) or building chosen from scene screen
     * @param buildingType building type with shared value of CallbackState
     * @param context specified object was chosen
     * @param builtAlready flag chosen from game screen or from build menu
     */
    void buildingChosen(const CallbackState &buildingType, Drawable* context, bool builtAlready);

    MainLoop* m_MainLoop;
    CallbackState m_CurrentState;
    SDL_Event* m_Event;
    int32_t m_MouseMoveTicks;
    bool m_BuildMenuOpened;
    bool m_GameMenuOpened;
    bool m_TempObjectDrawingState;
    Drawable* m_Selected;
    ObjectType m_SelectedType;
};

#endif //FALLBUILD_STATE_MACHINE_H
