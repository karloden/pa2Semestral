#ifndef FALLBUILD_GAME_H
#define FALLBUILD_GAME_H

#include <vector>
#include <atomic>
#include <thread>
#include <mutex>
#include "misc.h"
#include "drawable.h"

/**
 * \brief provides game logic and operating with game statistics as money, people etc.
 */
class GameLogic {
public:
    /**
     * constructs instance when new game is started (all values initialized by 0, but start money initialized from config file)
     * @param gameOverFlag atomic boolean used to indicate state of game, when game is over, injects in thread
     */
    GameLogic(std::atomic_bool &gameOverFlag);

    /**
     *
     * @param gameOverFlag atomic boolean used to indicate state of game, when game is over, injects in thread
     * @param serialized array size of 7 values of atomic_long, created by another instance to reinitialize that state in another time
     */
    GameLogic(std::atomic_bool &gameOverFlag, const void* serialized);

    /**
     * \brief destructor primary used to join thread after end of the game
     */
    ~GameLogic() { m_CounterThread.join(); }

    /**
     * \brief changes stats, that building can give like something per sec.
     * @param building represents building added on scene in game logic
     */
    void buildingAdded(ObjectType building);

    /**
     * \brief changes stats, that building can give like something per sec.
     * @param building represents building removed from scene in game logic, changes stats, that building can give
     */
    void buildingRemoved(ObjectType building);

    /**
     * @param building parameter, used to determine building type
     * @return true if player has enough money to build
     */
    bool enoughMoney(ObjectType building) const { return m_Money >= m_BuildingParameters[building]->m_Cost; }

    /**
     * @return const pointer to money value, used to print it on GUI
     */
    const std::atomic_long* getMoneyPtr() const { return &m_Money; }

    /**
     * @return const pointer to happiness value, used to print it on GUI
     */
    const std::atomic_long* getHappinessPtr() const { return &m_Happiness; }

    /**
     * @return const pointer to crime value, used to print it on GUI
     */
    const std::atomic_long* getCrimePtr() const { return &m_Crime; }

    /**
     * @return const pointer to people count value, used to print it on GUI
     */
    const std::atomic_long* getPeoplePtr() const { return &m_People; }

    /**
     * \brief method used to transform values of current game to save to file
     * @return array of size 7 values of atomic_long
     */
    void* serialize();

private:

    /**
     * \brief method used to init new thread to count game statistics
     * @param gameOverFlag injected flag, that indicates when game is over due to all people death or due to player's desire to end game
     */
    void valuesCounter(std::atomic_bool &gameOverFlag);

    /**
     * reference to building parameters, used from parameters singleton class, used to determine stats of each building
     */
    const std::vector<Parameters::BuildingParameters*> &m_BuildingParameters;

    std::mutex m_ChangerMutex;
    std::thread m_CounterThread;
    std::atomic_long m_Money, m_MoneyPS;
    std::atomic_long m_Happiness, m_HappinessPS;
    std::atomic_long m_Crime, m_CrimePS;
    std::atomic_long m_People;
};

/**
 * \brief structure used to specify action when object is held to build.
 *	Can be used in different states to determine 4 next actions.
 */
struct ObjHoldingSemantics {
    /**
     * \brief enumerator used to determine next action
     */
    enum class HoldingAction : int {
        HoldInit = 0, HoldRelease, HoldMove, HoldPlace, HoldUninitialized
    };

    /**
     * \brief default constructor used to determine object place or release, due to it has no additional params
     * @param action determines action from enumerator
     */
    ObjHoldingSemantics(HoldingAction action = HoldingAction::HoldUninitialized) :
            m_Action(action), m_Object(SpritesEnd) {}

    /**
     * \brief constructor used to determine object been taken from list to place
     * @param action determines action from enumerator (logically object take here)
     * @param object represents object(building), that was taken from list to place
     */
    ObjHoldingSemantics(HoldingAction action, ObjectType object) :
            m_Action(action), m_Object(object), m_PosX(-200), m_PosY(-200) {}

    /**
     * \brief constructor used to determine move of object, that follows the mouse
     * @param action determines action from enumerator
     * @param x coordinate, used to determine where object ought to be placed
     * @param y coordinate, used to determine where object ought to be placed
     */
    ObjHoldingSemantics(HoldingAction action, int32_t x, int32_t y) :
            m_Action(action), m_Object(SpritesEnd), m_PosX(x), m_PosY(y) {}

    /**
     * \brief destructor used to possibly delete drawable object of holding object
     */
    ~ObjHoldingSemantics() { if (m_HoldingSceneObj) delete m_HoldingSceneObj; }

    /**
     * \brief sets obj, being selected to afterwards drawing to scene and it's size
     */
    void setHoldingSceneObj(Drawable* scObj, int32_t size);

    const HoldingAction m_Action;
    const ObjectType m_Object;
    Drawable* m_HoldingSceneObj = nullptr;
    int32_t m_PosX, m_PosY, m_Size;
};

#endif //FALLBUILD_GAME_H
