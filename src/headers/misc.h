#ifndef FALLBUILD_UTILITIES_H
#define FALLBUILD_UTILITIES_H

#include <string>
#include <zconf.h>
#include <algorithm>

/**
 * \brief used to correctly find mandatory resources, no matter how and where from application was executed
 * @return string, representing full path to binary
 */
std::string getPathToBinary();

/**
 * \brief takes a string to convert '\b' symbols as actual char erasing.
 * Used to get rid of odd behaviour of SDL_ShowMessageBox
 * @param str input string
 * @return output string
 */
std::string convertBackspaces(const std::string &str);

const int g_BuildingsCount = 9;

/**
 * \brief enum used to navigate throw sprites array to take easily wanted sprite and to cast it to CallbackState.
 * Casting to CallbackState helps to determine which type of building was selected, providing ObjectType as
 * CallbackState as Drawable's callback
 */
enum ObjectType : int {
    BuildingApartments = 0, BuildingRestaurant, BuildingPub,
    BuildingSupermarket, BuildingPolice, BuildingHospital,
    BuildingBrothel, BuildingSkyscraper, BuildingBank,
    SpriteEnvTreeL, SpriteEnvGround, SpriteEnvSand, SpriteEnvPalm,
    SpriteEnvTreeN, SpriteEnvGrass, SpriteIntButton, SpriteIntInterface,
    SpriteEnvReserve, SpriteIntLogo, SpritesEnd,
};

/**
 * \brief enum class used for both: realize callbacks from buttons and save current state of application
 * value for first enumerated state can't collide with ObjectType 's, in order to each object can return
 * it's ObjectType as Callback, used to determine which type of object was selected
 */
enum class CallbackState : int {
    StCbMenu = 256, StCbGame, StCbGameHoldObject,
    StCbSaveGame, StCbLoadGame, CbReleaseSelected,
    CbNone, CbGameBuildMenu, CbGameMenu,
    CbExit, CbMenuFromGame
};

/**
 * \brief simply identifies which corner should be used
 */
enum class Corner : int {
    LeftUp = 0, RightUp, LeftDown, RightDown
};


#endif //FALLBUILD_UTILITIES_H
