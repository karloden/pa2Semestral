#ifndef FALLBUILD_DISPLAY_H
#define FALLBUILD_DISPLAY_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <atomic>
#include "parameters.h"
#include "sprites.h"
#include "drawable.h"
#include "game_logic.h"

extern const std::string g_SpritesPath;
extern const std::string g_LogoPath;

//Predefining MainLoop class in order to have it in friends of Display
class MainLoop;

/**
 * \brief class used to display GUI and read gui-based input
 */
class Display {
    friend MainLoop;
private:
    /**
     * \brief default constructor used to initialize mandatory variables and allocate space of first dimension for 2D vector [layer][object]
     */
    Display();

    /**
     * \brief initializes all SDL modules, that are needed by program
     * @param windowName accepts name of main window
     */
    void initializeGUI(const std::string &windowName);

    /**
     * \brief destructor used to free memory of SDL modules and free all memory allocated by display elements
     */
    ~Display();

    /**
     * \brief draws scene objects by separate layers, if layer is drawn, all layers above will be drawn too
     * @param layer specifies layer, from which drawing will be started
     */
    void drawSceneObjects(int8_t layer = LayersEnd) const;

    /**
     * \brief temporarily draws new object to scene directly, that doesn't affect performance, due to not drawing all scene every next tick
     * Anyway interface is ought to be drawn after temp object
     * @param object specifies object, that will temporarily appear
     */
    void drawTempObject(const Drawable* object) const;

    /**
     * \brief loads sprites map from disk and cuts it to separate sprites, afterwards loads them directly to GPU RAM
     */
    void loadSprites();

    /**
     * \brief cleans render buffer-texture with black color
     */
    void cleanDisplay();

    /**
     * \brief completely removes current scene objects from memory and resets display-states like hovering and cursor
     */
    void resetDisplay();

    /**
     * \brief initialize game display objects, such as game GUI and map background (ground and trees)
     * @param gameLogic injected gameLogic instance pointer, used to get const pointers to print game stats on screen
     */
    void addGameObjToScene(const GameLogic* gameLogic);

    /**
     * \brief initialize menu display objects(logo, buttons)
     */
    void addMenuObjToScene();

    /**
     * \brief places GUI field on screen corner
     * @param corner enum indicator of field position
     * @param w width of interface field in squares
     * @param h height of interface field in squares
     */
    void addInterfaceFields(const Corner &corner, int32_t w, int32_t h);

    /**
     * \brief add ground tiles and trees to scene according to environment var read from config
     * @param first ground tile identifier used more
     * @param second ground tile identifier used less
     */
    void addGround(int32_t first, int32_t second);

    /**
     * \brief places building to the scene if it's not colliding anything
     * @param x coordinate of placing
     * @param y coordinate of placing
     * @param type enum type of building that has to be planted
     * @return
     */
    bool placeNewBuilding(int32_t x, int32_t y, ObjectType type);

    /**
     * \brief writes every building's 3 vars, that are needed to reinitialized game from a file
     * @param saveFile stream, used to write a buildings data
     */
    void saveBuildingsToFile(std::ofstream &saveFile) const;

    /**
     * \brief deletes building from scene by searching it throw objects layer and erasing in from from there.
     * Also deallocates memory for this drawable object
     * @param object object ought to be deleted
     * @return
     */
    bool deleteBuilding(Drawable* object);

    /**
     * \brief takes coordinates where mouse cursor is and hover underlying object
     * @param x coordinate of place where hovering should be
     * @param y coordinate of place where hovering should be
     */
    void hoverObject(int32_t x, int32_t y);

    /**
     * \brief cancels object hovering if present
     */
    void clearHovered();

    /**
     * \brief changes entering coordinates parameters to begin of connected tile depending on sprite size
     * Used to determine to which one tile building will be "adhered"
     * @param xTilePos x coordinate, returns value here
     * @param yTilePos y coordinate, returns value here
     * @param size size of building sprite for which one calculating was requested
     */
    void calculateLinkedTile(int32_t &xTilePos, int32_t &yTilePos, int32_t size) const;

    /**
     * \brief simply disables and enables all gui and game scene objects' clickability
     * @param disabled determines which exactly state should objects accept
     */
    void switchClickability(bool disabled);

    /**
     * \brief shows and hides build menu.
     * MUST be called only if game menu is not opened!
     * @param enable flag which determines if show or hide menu
     */
    void buildMenu(bool enable);

    /**
     * \brief shows and hides game menu.
     * MUST be called only if build menu is not opened!
     * @param enable flag which determines if show or hide menu
     */
    void gameMenu(bool enable);

    /**
     * There are 3 layers to display on. First for ground only, second for trees and buildings. Last for interface
     */
    static const int8_t LayerBackground = 0;
    static const int8_t LayerObjects = 1;
    static const int8_t LayerInterface = 2;
    static const int8_t LayersEnd = 3;
    std::vector<std::vector<Drawable*>> m_SceneObjects;
    std::vector<Sprite*> m_Sprites;
    int32_t m_CameraPosX, m_CameraPosY;
    Drawable* m_CurrentlyHovered;
    SDL_Window* m_Window;
    SDL_Renderer* m_Renderer;
    SDL_Texture* m_FinalRenderTexture;
    SDL_Cursor* m_CursorHand;
    SDL_Cursor* m_CursorDefault;
};

#endif //FALLBUILD_DISPLAY_H
