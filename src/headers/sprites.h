#ifndef FALLBUILD_SPRITES_H
#define FALLBUILD_SPRITES_H

#include <SDL2/SDL.h>
#include <string>
#include "misc.h"

/**
 * \brief class works as a wrapper for SDL_Texture, in order to get their size and switch object's hovering
 */
class Sprite {
public:
    /**
     * \brief constructs new instance of sprite
     * @param kind takes type of this object
     * @param texture first copy of texture used as plain object
     * @param textureCopy second copy of texture is greened that represents "mouse hovering"
     */
    Sprite(const ObjectType &kind, SDL_Texture* texture, SDL_Texture* textureCopy);

    /**
     * \brief frees memory
     */
    ~Sprite();

    /**
     * @return resolution of object
     */
    uint32_t getRes() const;

    /**
     * \brief allows to get texture depending on final object is hovered or not
     * @param hovered flag, shows if object is hovered
     * @return texture
     */
    SDL_Texture* getTexture(bool hovered) const;

private:
    const ObjectType m_Kind;
    SDL_Texture* m_Texture;
    SDL_Texture* m_TextureHovered;
};

#endif //FALLBUILD_SPRITES_H
