#ifndef FALLBUILD_DRAWABLE_H
#define FALLBUILD_DRAWABLE_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <atomic>
#include <functional>
#include <sstream>
#include "parameters.h"
#include "sprites.h"


extern const std::string g_MainFontPath;
extern const std::string g_MainItalFontPath;

/**
 * \brief class defines primary methods and primary variables that every object ought to have.
 *	This parent class doesn't provide any member vars used to actual draw, every inherited must
 *	Every object has a callback action on click, no matter if it is clickable. If not,
 *	object shouldn't be controlled at all for point lying in it's set
 */
class Drawable {
public:
    /**
     * \brief constructor that initializes all Drawable class values
     * @param x coordinate X on whole game map (screenwise if relates to for example to GUI)
     * @param y coordinate Y on whole game map (screenwise if relates to for example to GUI)
     * @param w sprite width
     * @param h sprite height
     * @param relativePosX pointer to value, shows relational X axis pos(for example camera pos), NULL if static
     * @param relativePosY pointer to value, shows relational Y axis pos(for example camera pos), NULL if static
     * @param clickable true if object can be clicked
     * @param callback enum value, can be also casted buildable object, that shows what action ought to be made on click
     */
    Drawable(uint32_t x, uint32_t y, uint32_t w, uint32_t h, int32_t* relativePosX, int32_t* relativePosY,
             bool clickable, CallbackState callback);

    /**
     * \brief virtual destructor. Does nothing in Drawable parent class (nothing to free)
     */
    virtual ~Drawable() {}

    /**
     * @return true if object is clickable
     */
    bool isClickable() const { return m_Clickable; }

    /**
     * @return enumerator class value, action, that should be done on click
     */
    CallbackState callbackAction() const { return m_Callback; }

    /**
     * \brief prerequisite is that we don't use multiple windows to draw objects, so all drawable object can share one renderer
     * @param renderer renderer value used to pass it to SDL functions
     */
    static void setRenderer(SDL_Renderer* renderer) { m_Renderer = renderer; }

    /**
     * \brief checks if passed coordinates belongs to 'this' object
     * @param x X axis coordinate
     * @param y Y axis coordinate
     * @return true if coordinates belongs
     */
    bool inSetOf(int32_t x, int32_t y) const;

    /**
     * \brief check if 'this' objects collides with passed as a parameter
     * Prerequisite is, that object are rectangles, so actually method checks, that every single angle is outside
     * other's surface
     * @param other
     * @return true if objects collides
     */
    bool collides(const Drawable &other) const;

    /**
     * \brief disables and enables object's clickability, uses another flag to save default clickability
     * @param disabled true if object's clickability is ought to be disabled
     */
    void disableClickable(bool disabled);

    /**
     * \brief blocks object's hover state, in order to set it 'selected'
     * @param enabled true if object was 'selected'
     */
    void hoveringToSelected(bool enabled);

    /**
     * \brief serializes object's primary 3 values, that are copied binary to array.
     * Primarily is used to save building state, so it's possible to get building's properties with only callback value
     * @return array of size 3 * sizeof(int)
     */
    void* serialize();

    /**
     * \brief virtual method that ought to be implemented in inherited classes
     */
    virtual void draw() const = 0;

    /**
     * \brief virtual method that ought to be implemented in inherited classes
     * @param hovered flag that shows if hovering must be enabled(true)
     */
    virtual void setHovering(bool hovered) = 0;

protected:
    static SDL_Renderer* m_Renderer;
    SDL_Rect m_Pos;
    const int32_t* m_XRelative, * m_YRelative;
    CallbackState m_Callback;
    bool m_Clickable;
    bool m_Hovered;
    bool m_DefClickable;
    bool m_HoverLocked;
};

/**
 * \brief class used to print any text non-changable info on GUI or game screen space
 */
class TextInfo : public Drawable {
public:
    /**
     * \brief initializes TextInfo instance
     * @param x coordinate X on whole game map (screenwise if relates to for example to GUI)
     * @param y coordinate Y on whole game map (screenwise if relates to for example to GUI)
     * @param fontSize size of font - height of one symbol in pixels
     * @param text text that ought to be printed
     * @param color color of text, all values of SDL_Color struct should be initialized
     * @param relativePosX pointer to value, shows relational X axis pos(for example camera pos), NULL if static
     * @param relativePosY pointer to value, shows relational Y axis pos(for example camera pos), NULL if static
     * @param true if italic
     */
    TextInfo(uint32_t x, uint32_t y, uint32_t fontSize, const std::string &text, const SDL_Color &color,
             int32_t* relativePosX, int32_t* relativePosY, bool ital);

    /**
     * \brief destructor free memory allocated for text texture
     */
    virtual ~TextInfo() { SDL_DestroyTexture(m_CookedText); }

    /**
     * \brief draws object using relative constants. Does nothing if object doesn't lay on screen surface
     */
    virtual void draw() const;

    /**
     * \brief does nothing due to text info can't be hovered
     */
    virtual void setHovering(bool hovered) {}

protected:
    SDL_Texture* m_CookedText;
};

/**
 * \brief class used to print info on interface such like statistics
 */
class StaticTextInfo : public TextInfo {
public:
    /**
     * \brief initializes StaticTextInfo instance
     * @param x coordinate X on whole game map (screenwise if relates to for example to GUI)
     * @param y coordinate Y on whole game map (screenwise if relates to for example to GUI)
     * @param fontSize size of font - height of one symbol in pixels
     * @param text text that ought to be printed
     * @param color color of text, all values of SDL_Color struct should be initialized
     * @param printValue pointer to atomic_long value, that will be printed after text in format "%s: %ld"
     * @param true if italic
     */
    StaticTextInfo(uint32_t x, uint32_t y, uint32_t fontSize, const std::string &text, const SDL_Color &color,
                   const std::atomic_long* printValue, bool ital);

    /**
     * \brief deinitialize font
     */
    virtual ~StaticTextInfo() { TTF_CloseFont(m_Font); }

    /**
     * \brief draws text with value in format "%s: %ld"
     */
    virtual void draw() const;

private:
    uint8_t getNumberOfDigits(int64_t number) const;

    const std::atomic_long* m_Value;
    TTF_Font* m_Font;
    SDL_Color m_Color;
};

/**
 * \brief class used to make buttons with text on interface
 */
class TextButton : public TextInfo {
public:
    /**
     * \brief initializes instance
     * @param x coordinate X on whole game map (screenwise if relates to for example to GUI)
     * @param y coordinate Y on whole game map (screenwise if relates to for example to GUI)
     * @param fontSize size of font - height of one symbol in pixels
     * @param text text that ought to be printed
     * @param color color of text, all values of SDL_Color struct should be initialized
     * @param callback represents action, that will be dont on object click
     * @param buttonSprite pointer to sprite object, if background of button won't be blank black
     */
    TextButton(uint32_t x, uint32_t y, uint32_t fontSize, const std::string &text, const SDL_Color &color,
               CallbackState callback, Sprite* buttonSprite);

    virtual ~TextButton() { SDL_DestroyTexture(m_CookedTextHovered); }

    /**
     * \brief switches object hovering
     * @param hovered represents object will be hovered or not
     */
    virtual void setHovering(bool hovered);

protected:
    SDL_Texture* m_CookedTextHovered;
};

/**
 * \brief class used to add static objects to scene
 */
class StaticObject : public Drawable {
public:

    /**
     * \brief initializes StaticObject instance
     * @param x coordinate X on whole game map (screenwise if relates to for example to GUI)
     * @param y coordinate Y on whole game map (screenwise if relates to for example to GUI)
     * @param relativePosX pointer to value, shows relational X axis pos(for example camera pos), NULL if static
     * @param relativePosY pointer to value, shows relational Y axis pos(for example camera pos), NULL if static
     * @param sprite pointer to sprite object, that will be drawn, can not be NULL
     * @param size if object won't be default size, size in pixels should be passed here, otherwise 0
     */
    StaticObject(uint32_t x, uint32_t y, int32_t* relativePosX, int32_t* relativePosY, Sprite* sprite, uint32_t size);

    virtual ~StaticObject() {}

    /**
     * \brief draws object if it is in borders of screen
     */
    virtual void draw() const;

    /**
     * \brief switches object hovering
     * @param hovered represents object will be hovered or not
     */
    virtual void setHovering(bool hovered) {}

protected:
    Sprite* m_Sprite;
};

/**
 * \brief class used to add building objects to scene
 */
class BuildingDisplayObject : public StaticObject {
public:
    /**
     * \brief initializes StaticObject instance
     * @param x coordinate X on whole game map (screenwise if relates to for example to GUI)
     * @param y coordinate Y on whole game map (screenwise if relates to for example to GUI)
     * @param relativePosX pointer to value, shows relational X axis pos(for example camera pos), NULL if static
     * @param relativePosY pointer to value, shows relational Y axis pos(for example camera pos), NULL if static
     * @param sprite pointer to sprite object, that will be drawn, can not be NULL
     * @param callback shows which exactly type of building is going to be added
     * @param size if object won't be default size, size in pixels should be passed here, otherwise 0
     */
    BuildingDisplayObject(uint32_t x, uint32_t y, int32_t* relativePosX, int32_t* relativePosY, Sprite* sprite,
                          ObjectType callback, uint32_t size);

    virtual ~BuildingDisplayObject() {}

    /**
     * \brief switches object hovering
     * @param hovered represents object will be hovered or not
     */
    virtual void setHovering(bool hovered);
};


#endif //FALLBUILD_DRAWABLE_H
