#ifndef FALLBUILD_EXCEPTIONS_H
#define FALLBUILD_EXCEPTIONS_H


#include <exception>
#include <string>
#include "misc.h"

#include <SDL2/SDL.h>

/**
 * \brief constructs runtime or logic exception.
 * Used exception not inherited from std::exception due to odd SDL library displaying in MessageBox
 */
class MessageException {
public:
    /**
     * @param message accepts message, that will be saved and accessible by calling what()
     */
    MessageException(const std::string &message) : m_Message(convertBackspaces(message)) {}

    virtual ~MessageException() {}

    /**
     * @return exception message
     */
    std::string what() const { return m_Message; }

private:
    const std::string m_Message;
};

/**
 * \brief represents exception, thrown by unexpectable SDL behavior
 */
class SDLException : public MessageException {
public:
    SDLException() : MessageException("SDL2 library internal error: " + std::string(SDL_GetError())) {}

    virtual ~SDLException() {}
};

/**
 * \brief represents exception, thrown when config file has invalid format
 */
class ConfigFileFormatException : public MessageException {
public:
    ConfigFileFormatException(const std::string &fileName = "\b",
                              const std::string &wrongString = "\b\b.") :
            MessageException(fileName +
                             " has wrong format! Unexpected or invalid entry found: " + wrongString) {}

    virtual ~ConfigFileFormatException() {}


};

/**
 * \brief represents exception, thrown when mandatory file was't correctly opened
 */
class FileOpenException : public MessageException {
public:
    FileOpenException(const std::string &fileName = "\b") :
            MessageException("Error opening file " + fileName) {}

    virtual ~FileOpenException() {}

};


#endif //FALLBUILD_EXCEPTIONS_H
