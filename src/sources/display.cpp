#include "../headers/display.h"

const int8_t Display::LayerBackground;
const int8_t Display::LayerObjects;
const int8_t Display::LayerInterface;
const int8_t Display::LayersEnd;

Display::Display() : m_CurrentlyHovered(nullptr), m_Window(nullptr),
                     m_Renderer(nullptr) { m_SceneObjects.resize(LayersEnd); }


void Display::initializeGUI(const std::string &windowName) {
    if (SDL_Init(SDL_INIT_EVERYTHING)) throw SDLException();
    if (!(m_Window = SDL_CreateWindow(windowName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                      Parameters::getInstance().resX(), Parameters::getInstance().resY(),
                                      (Parameters::getInstance().fullscreen() ? SDL_WINDOW_FULLSCREEN : 0))) ||
        !(m_Renderer = SDL_CreateRenderer(m_Window, -1, SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED)) ||
        !(m_CursorHand = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND)))
        throw SDLException();
    if (!IMG_Init(IMG_INIT_PNG) || TTF_Init() < 0) throw MessageException("SDL modules failed to load.");
    m_FinalRenderTexture = SDL_CreateTexture(m_Renderer, SDL_PIXELFORMAT_RGBA8888,
                                             SDL_TEXTUREACCESS_TARGET, Parameters::getInstance().resX(),
                                             Parameters::getInstance().resY());
    if (!m_FinalRenderTexture) throw SDLException();
    loadSprites();
    m_CursorDefault = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW);
    SDL_SetCursor(m_CursorDefault);
    Drawable::setRenderer(m_Renderer);
}


void Display::addMenuObjToScene() {
    resetDisplay();
    SDL_Color white;
    white.r = white.g = white.b = 255;
    white.a = 0;
    uint32_t oneSymbolH = (uint32_t) Parameters::getInstance().resY() / 6;
    uint32_t oneSymbolW = (uint32_t) (oneSymbolH / 2.25);
    uint32_t offset = m_Sprites[SpriteIntLogo]->getRes() / 3;
    m_SceneObjects[LayerInterface].push_back(
            new StaticObject((uint32_t) (Parameters::getInstance().resX() - m_Sprites[SpriteIntLogo]->getRes()) / 2,
                             oneSymbolH / 8, nullptr, nullptr, m_Sprites[SpriteIntLogo], 0));
    m_SceneObjects[LayerInterface].push_back(
            new TextButton((Parameters::getInstance().resX() - 8 * oneSymbolW) / 2, oneSymbolH * 1 + offset, oneSymbolH,
                           "New Game", white, CallbackState::StCbGame, nullptr));
    m_SceneObjects[LayerInterface].push_back(
            new TextButton((Parameters::getInstance().resX() - 9 * oneSymbolW) / 2, oneSymbolH * 2 + offset, oneSymbolH,
                           "Load Game", white, CallbackState::StCbLoadGame, nullptr));
    m_SceneObjects[LayerInterface].push_back(
            new TextButton((Parameters::getInstance().resX() - 4 * oneSymbolW) / 2, oneSymbolH * 3 + offset, oneSymbolH,
                           "Exit", white, CallbackState::CbExit, nullptr));

    drawSceneObjects(LayerBackground);
}


void Display::addInterfaceFields(const Corner &corner, int32_t w, int32_t h) {
    uint32_t initX = 0, initY = 0;
    uint32_t oneSquareSize = m_Sprites[SpriteIntInterface]->getRes();
    switch (corner) {
        case Corner::LeftUp: break;
        case Corner::RightUp: initX = Parameters::getInstance().resX() - oneSquareSize * w;
            break;
        case Corner::RightDown: initX = Parameters::getInstance().resX() - oneSquareSize * w;
        case Corner::LeftDown: initY = Parameters::getInstance().resY() - oneSquareSize * h;
            break;
    }
    for (uint8_t x = 0; x < w; x++)
        for (uint8_t y = 0; y < h; y++)
            m_SceneObjects[LayerInterface].push_back(
                    new StaticObject(initX + x * oneSquareSize, initY + y * oneSquareSize,
                                     nullptr, nullptr, m_Sprites[SpriteIntInterface], 0));
}


void Display::addGameObjToScene(const GameLogic* gameLogic) {
    resetDisplay();
    uint32_t oneSquareSize = m_Sprites[SpriteIntInterface]->getRes();
    int32_t resX = Parameters::getInstance().resX(), resY = Parameters::getInstance().resY();
    SDL_Color white;
    white.r = white.g = white.b = 255;
    SDL_Color black;
    black.r = black.g = black.b = 0;
    addGround(Parameters::getInstance().getEnvironmentFirst(), Parameters::getInstance().getEnvironmentSecond());
    addInterfaceFields(Corner::LeftDown, 5, 2);
    addInterfaceFields(Corner::RightDown, 5, 3);
    m_SceneObjects[LayerInterface].push_back(
            new StaticTextInfo(resX - 5 * oneSquareSize + 15, resY - 28 * oneSquareSize / 10,
                               50, "Money", white, gameLogic->getMoneyPtr(), true));
    m_SceneObjects[LayerInterface].push_back(
            new StaticTextInfo(resX - 5 * oneSquareSize + 15, resY - 22 * oneSquareSize / 10,
                               50, "People", white, gameLogic->getPeoplePtr(), true));
    m_SceneObjects[LayerInterface].push_back(
            new StaticTextInfo(resX - 5 * oneSquareSize + 15, resY - 16 * oneSquareSize / 10,
                               50, "Happy", white, gameLogic->getHappinessPtr(), true));
    m_SceneObjects[LayerInterface].push_back(
            new StaticTextInfo(resX - 5 * oneSquareSize + 15, resY - 9 * oneSquareSize / 10,
                               50, "Crime", white, gameLogic->getCrimePtr(), true));
    //It's important to add buttons in very end.
    m_SceneObjects[LayerInterface].push_back(
            new TextButton(oneSquareSize * 2 / 5, resY - 3 * oneSquareSize / 2, 40, " Build ", black,
                           CallbackState::CbGameBuildMenu, m_Sprites[SpriteIntButton]));
    m_SceneObjects[LayerInterface].push_back(
            new TextButton(oneSquareSize * 11 / 4, resY - 3 * oneSquareSize / 2, 40, " Menu ", black,
                           CallbackState::CbGameMenu, m_Sprites[SpriteIntButton]));
    drawSceneObjects(LayerBackground);
}


void Display::loadSprites() {
    //Resolves an order of objects, used by enumerator ObjectType values
    const int memoryOrder[] = {3, 7, 2, 6, 4, 9, 10, 11, 12, 13, 14, 15, 16, 17, 0, 5, 8, 1};
    SDL_Surface* spritesRaw = IMG_Load(g_SpritesPath.c_str());
    if (!spritesRaw) throw FileOpenException(g_SpritesPath);
    SDL_Rect rectSmall, rectMedium, rectLarge;
    rectSmall.x = rectSmall.y = rectMedium.x = rectMedium.y = rectLarge.x = rectLarge.y = 0;
    rectSmall.w = rectSmall.h = spritesRaw->w / 4;
    rectLarge.w = rectLarge.h = spritesRaw->w / 2;
    rectMedium.w = rectMedium.h = (spritesRaw->w - rectSmall.w) / 2;
    Parameters::getInstance().setTexturesSizes((uint32_t) rectSmall.w, (uint32_t) rectMedium.w, (uint32_t) rectLarge.w);
    SDL_Rect* rect = nullptr;
    m_Sprites.resize(SpritesEnd, nullptr);
    //Used -1 in whole cycle max value counters due to last loaded logo sprite
    for (int spriteNumber = 0; spriteNumber < SpritesEnd - 1; spriteNumber++) {
        int32_t posX = 0, posY = 0;
        if (rect) {
            posX = (rect->x + rect->w) % spritesRaw->w;
            posY = ((rect->x + rect->w) / spritesRaw->w) == 1 ? (rect->y + rect->h) : rect->y;
            if (rect == &rectMedium && ((rect->x + rect->w + rectSmall.w) / spritesRaw->w) == 1) {
                posX = 0;
                posY = rect->y + rect->h;
            }
        }
        //Sprites bigger than default size (not 1/4 of map width) are listed here
        if (spriteNumber >= 0 && spriteNumber <= 1)
            rect = &rectLarge;
            //Last 4 sprites are medium (2 medium and 1 small are fitted in map width)
        else if (spriteNumber >= (SpritesEnd - 4) - 1 && spriteNumber <= SpritesEnd - 1)
            rect = &rectMedium;
        else rect = &rectSmall;
        rect->x = posX;
        rect->y = posY;
        SDL_Surface* finalSurface = SDL_CreateRGBSurface(0, rect->w, rect->h, 32,
                                                         0x000000FF, 0x0000FF00, 0x00FF0000, 0xFF000000);
        SDL_BlitSurface(spritesRaw, rect, finalSurface, nullptr);
        SDL_Texture* texture = SDL_CreateTextureFromSurface(m_Renderer, finalSurface);
        SDL_Texture* textureCopied = SDL_CreateTextureFromSurface(m_Renderer, finalSurface);
        SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
        SDL_SetTextureBlendMode(textureCopied, SDL_BLENDMODE_BLEND);
        m_Sprites[memoryOrder[spriteNumber]] = new Sprite((ObjectType) memoryOrder[spriteNumber], texture,
                                                          textureCopied);
        SDL_FreeSurface(finalSurface);
    }
    SDL_FreeSurface(spritesRaw);
    SDL_Surface* logoSurfaceRaw = IMG_Load(g_LogoPath.c_str());
    if (!logoSurfaceRaw) throw FileOpenException(g_LogoPath);
    SDL_Texture* logoTexture = SDL_CreateTextureFromSurface(m_Renderer, logoSurfaceRaw);
    SDL_SetTextureBlendMode(logoTexture, SDL_BLENDMODE_BLEND);
    m_Sprites[SpriteIntLogo] = new Sprite(SpriteIntLogo, logoTexture, nullptr);
    SDL_FreeSurface(logoSurfaceRaw);
}


Display::~Display() {
    resetDisplay();
    SDL_DestroyTexture(m_FinalRenderTexture);
    SDL_FreeCursor(m_CursorDefault);
    SDL_FreeCursor(m_CursorHand);
    for (auto &a:m_Sprites) delete a;
    SDL_DestroyRenderer(m_Renderer);
    SDL_DestroyWindow(m_Window);
    if (TTF_WasInit())
        TTF_Quit();
    IMG_Quit();
    if (SDL_WasInit(SDL_INIT_EVERYTHING))
        SDL_Quit();
}


void Display::resetDisplay() {
    cleanDisplay();
    for (auto &layer:m_SceneObjects) {
        for (auto &obj:layer)
            delete obj;
        layer.clear();
    }
    m_CurrentlyHovered = nullptr;
    SDL_SetCursor(m_CursorDefault);
}

void Display::cleanDisplay() {
    SDL_SetRenderTarget(m_Renderer, m_FinalRenderTexture);
    SDL_SetRenderDrawColor(m_Renderer, 0, 0, 0, 0);
    SDL_RenderClear(m_Renderer);
    SDL_SetRenderTarget(m_Renderer, nullptr);
}


void Display::drawSceneObjects(int8_t layer) const {
    SDL_SetRenderTarget(m_Renderer, m_FinalRenderTexture);
    switch (layer) {
        case LayerBackground: for (auto &obj:m_SceneObjects[LayerBackground]) obj->draw();
        case LayerObjects: for (auto &obj:m_SceneObjects[LayerObjects]) obj->draw();
        case LayerInterface: for (auto &obj:m_SceneObjects[LayerInterface]) obj->draw();
        default: break;
    }
    SDL_SetRenderTarget(m_Renderer, nullptr);
    SDL_RenderCopy(m_Renderer, m_FinalRenderTexture, nullptr, nullptr);
    SDL_RenderPresent(m_Renderer);
}


void Display::drawTempObject(const Drawable* object) const {
    SDL_RenderCopy(m_Renderer, m_FinalRenderTexture, nullptr, nullptr);
    object->draw();
    for (auto &obj:m_SceneObjects[LayerInterface]) obj->draw();
    SDL_RenderPresent(m_Renderer);
}

void Display::calculateLinkedTile(int32_t &xTilePos, int32_t &yTilePos, int32_t size) const {
    int32_t x = xTilePos + m_CameraPosX % size;
    int32_t y = yTilePos + m_CameraPosY % size;
    int32_t groundTileSize = m_Sprites[SpriteEnvGrass]->getRes();
    if (groundTileSize == size) {
        if (!((x * 2 / groundTileSize) % 2)) {
            xTilePos = ((x + size / 4) / groundTileSize) * groundTileSize - groundTileSize / 2;
            yTilePos = ((y / (groundTileSize / 2)) * (groundTileSize / 2));
        } else {
            xTilePos = ((x + size / 4) / groundTileSize) * groundTileSize;
            yTilePos = ((y / (groundTileSize / 2)) * (groundTileSize / 2)) - groundTileSize / 4;
        }
    } else if (size == (int32_t) Parameters::getInstance().textureSizeMedium() * 3) {
        xTilePos = ((x + size / 4) / groundTileSize) * groundTileSize - groundTileSize / 4;
        yTilePos = (((y - size / 2) / (groundTileSize / 2)) * (groundTileSize / 2));
    } else {
        xTilePos = ((x - size / 6) / groundTileSize) * groundTileSize;
        yTilePos = (((y - size / 3) / (groundTileSize / 2)) * (groundTileSize / 2));
    }
    xTilePos -= m_CameraPosX % size;
    yTilePos -= m_CameraPosY % size;
}


void Display::hoverObject(int32_t x, int32_t y) {
    for (int8_t layer = LayerInterface; layer > LayerBackground; --layer)
        for (auto &a:m_SceneObjects[layer])
            if (a->isClickable() && a->inSetOf(x, y)) {
                if (m_CurrentlyHovered == a) return;
                if (!m_CurrentlyHovered) {
                    a->setHovering(true);
                    SDL_SetCursor(m_CursorHand);
                    m_CurrentlyHovered = a;
                }
                if (m_CurrentlyHovered != a) {
                    m_CurrentlyHovered->setHovering(false);
                    a->setHovering(true);
                }
                m_CurrentlyHovered = a;
                drawSceneObjects(LayerObjects);
                return;
            }
    clearHovered();
}

void Display::addGround(int32_t first, int32_t second) {
    //"Minimum standard", recommended by Park, Miller, and Stockmeyer in 1993 from cppreference.com
    std::linear_congruential_engine<std::uint32_t, 48271, 0, 2147483647> pseudoRandomGenerator;
    int32_t groundTileSize = (uint32_t) m_Sprites[first]->getRes();
    m_CameraPosX = (Parameters::getInstance().mapW() / 2) - (Parameters::getInstance().resX() / 2);
    m_CameraPosY = (Parameters::getInstance().mapH() / 2) - (Parameters::getInstance().resY() / 2);
    for (int32_t j = 0; j < Parameters::getInstance().mapH() / (groundTileSize / 4) + 4; j++) {
        int32_t groundX = ((j % 2) * groundTileSize / 2) - groundTileSize * 2;
        int32_t groundY = (groundTileSize / 4 * j) - groundTileSize;
        for (int32_t i = 0; i < Parameters::getInstance().mapW() / (groundTileSize / 2); i++) {
            int32_t groundTile;
            switch (pseudoRandomGenerator() % 5) {
                case 0:
                case 1:
                case 2: groundTile = first;
                    break;
                default: groundTile = second;
            }
            m_SceneObjects[LayerBackground].push_back(
                    new StaticObject((uint32_t) groundX, (uint32_t) groundY, &m_CameraPosX, &m_CameraPosY,
                                     m_Sprites[groundTile], 0));
            uint64_t treeHere = pseudoRandomGenerator() % 50;
            if (!treeHere) {
                int32_t treeSprite;
                switch (first) {
                    case SpriteEnvGrass: treeSprite = SpriteEnvTreeL;
                        break;
                    case SpriteEnvGround: treeSprite = SpriteEnvTreeN;
                        break;
                    default: treeSprite = SpriteEnvPalm;
                }
                m_SceneObjects[LayerObjects].push_back(
                        new StaticObject((uint32_t) groundX, (uint32_t) groundY + groundTileSize / 4, &m_CameraPosX,
                                         &m_CameraPosY, m_Sprites[treeSprite], 0));
            }
            groundX += groundTileSize;
        }
    }
}

bool Display::deleteBuilding(Drawable* object) {
    for (auto iter = m_SceneObjects[LayerObjects].begin(); iter != m_SceneObjects[LayerObjects].end(); ++iter)
        if (object == *iter) {
            delete *iter;
            m_SceneObjects[LayerObjects].erase(iter);
            m_CurrentlyHovered = nullptr;
            drawSceneObjects(LayerBackground);
            return true;
        }
    return false;
}

void Display::switchClickability(bool disabled) {
    for (int8_t layer = LayerObjects; layer < LayersEnd; ++layer)
        for (auto &a:m_SceneObjects[layer]) a->disableClickable(disabled);
}

void Display::clearHovered() {
    if (m_CurrentlyHovered) {
        m_CurrentlyHovered->setHovering(false);
        SDL_SetCursor(m_CursorDefault);
        m_CurrentlyHovered = nullptr;
        drawSceneObjects(LayerObjects);
    }
}

bool Display::placeNewBuilding(int32_t x, int32_t y, ObjectType type) {
    Drawable* placedBld = new BuildingDisplayObject(x, y, &m_CameraPosX, &m_CameraPosY, m_Sprites[type], type, 0);
    for (const auto &obj:m_SceneObjects[LayerObjects])
        if (obj->collides(*placedBld)) {
            delete placedBld;
            return false;
        }
    m_SceneObjects[LayerObjects].push_back(placedBld);
    drawSceneObjects(LayerObjects);
    return true;
}

void Display::buildMenu(bool enable) {
    std::vector<Drawable*> &intObjects = m_SceneObjects[Display::LayerInterface];
    if (enable) {
        uint32_t buttonsSize = Parameters::getInstance().textureSizeSmall() * 2;
        uint32_t initPos = (Parameters::getInstance().resX() - g_BuildingsCount * (buttonsSize + 15)) / 2;
        SDL_Color white;
        white.r = white.g = white.b = 255;
        intObjects[intObjects.size() - 3]->hoveringToSelected(true);
        for (int building = 0; building < g_BuildingsCount; ++building) {
            //Adds building buttons to interface layer, for each prints cost, beneath or under
            intObjects.push_back(
                    new BuildingDisplayObject(building * (buttonsSize + 15) + initPos, 25, nullptr, nullptr,
                                              m_Sprites[building], (ObjectType) building, buttonsSize));
            int32_t cost = Parameters::getInstance().getBuildingParameters()[building]->m_Cost;
            std::string costInscription("Cost: " + std::to_string(cost));
            intObjects.push_back(
                    new TextInfo(building * (buttonsSize + 15) + initPos - buttonsSize / 4,
                                 buttonsSize * 3 * (building % 2) / 2, 25, costInscription, white,
                                 nullptr, nullptr, true));
        }
    } else {
        //assuming, that one menu couldn't be opened, before previous isn't closed
        for (auto iter = intObjects.end() - g_BuildingsCount * 2; iter != intObjects.end(); ++iter) delete *iter;
        intObjects.resize(intObjects.size() - g_BuildingsCount * 2);
        intObjects[intObjects.size() - 3]->hoveringToSelected(false);
    }
    drawSceneObjects(Display::LayerBackground);
}

void Display::gameMenu(bool enable) {
    std::vector<Drawable*> &intObjects = m_SceneObjects[Display::LayerInterface];
    uint32_t oneSquareSize = m_Sprites[SpriteIntInterface]->getRes();
    if (enable) {
        intObjects[intObjects.size() - 2]->hoveringToSelected(true);
        addInterfaceFields(Corner::RightUp, 4, 2);
        intObjects.push_back(
                new TextButton((uint32_t) (Parameters::getInstance().resX() - oneSquareSize * 3.7), 10, 45,
                               " Save Game ", SDL_Color(), CallbackState::StCbSaveGame,
                               m_Sprites[SpriteIntButton]));
        intObjects.push_back(
                new TextButton((uint32_t) (Parameters::getInstance().resX() - oneSquareSize * 3.7), 10 + oneSquareSize,
                               45,
                               " Main Menu ", SDL_Color(), CallbackState::CbMenuFromGame,
                               m_Sprites[SpriteIntButton]));
    } else {
        //assuming, that one menu couldn't be opened, before previous isn't closed
        for (auto iter = intObjects.end() - 10; iter != intObjects.end(); ++iter) delete *iter;
        intObjects.resize(intObjects.size() - 10);
        intObjects[intObjects.size() - 2]->hoveringToSelected(false);
    }
    drawSceneObjects(Display::LayerBackground);
}

void Display::saveBuildingsToFile(std::ofstream &saveFile) const {
    for (const auto &obj:m_SceneObjects[Display::LayerObjects]) {
        if ((ObjectType) obj->callbackAction() < g_BuildingsCount && (ObjectType) obj->callbackAction() >= 0) {
            void* sceneObject = obj->serialize();
            saveFile.write((char*) sceneObject, 3 * sizeof(int));
            delete[] (char*) sceneObject;
        }
    }
}
