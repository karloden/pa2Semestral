#include "../headers/main_loop.h"

MainLoop::MainLoop() : m_GameOver(true), m_ObjHolding(nullptr), m_GameLogic(nullptr) {
    try {
        Parameters::getInstance().readConfig();
        m_Display.initializeGUI(g_WindowName);
    } catch (const MessageException &a) {
        std::cout << a.what() << std::endl;
        m_IsClosed = true;
        //Shows simple exception message with "OK" button, that will exit application on click.
        displayMessageBox(a.what(), CallbackState::CbExit, "OK.");
        return;
    }
}

void MainLoop::deleteObject(Drawable* object, ObjectType type) {
    if (m_Display.deleteBuilding(object))
        m_GameLogic->buildingRemoved(type);
}

MainLoop::~MainLoop() {
    m_GameOver = true;
    if (m_GameLogic) delete m_GameLogic;
    if (m_ObjHolding) delete m_ObjHolding;
}

void MainLoop::resolveHolding(const ObjHoldingSemantics &semantics) {
    switch (semantics.m_Action) {
        case ObjHoldingSemantics::HoldingAction::HoldInit: {
            m_ObjHolding = new ObjHoldingSemantics(semantics);
            Sprite* connectedSprite = m_Display.m_Sprites[m_ObjHolding->m_Object];
            m_ObjHolding->setHoldingSceneObj(new BuildingDisplayObject(
                    0, 0, &m_ObjHolding->m_PosX, &m_ObjHolding->m_PosY, connectedSprite,
                    (ObjectType) CallbackState::StCbGameHoldObject, 0), connectedSprite->getRes());
            m_Display.switchClickability(true);
            m_Display.clearHovered();
            m_ObjHolding->m_HoldingSceneObj->setHovering(true);
            break;
        }
        case ObjHoldingSemantics::HoldingAction::HoldMove: {
            int32_t newPosX = semantics.m_PosX, newPosY = semantics.m_PosY;
            m_Display.calculateLinkedTile(newPosX, newPosY, m_ObjHolding->m_Size);
            if (-newPosX == m_ObjHolding->m_PosX && -newPosY == m_ObjHolding->m_PosY)
                return;
            m_ObjHolding->m_PosX = -newPosX;
            m_ObjHolding->m_PosY = -newPosY;
            m_ObjHolding->m_HoldingSceneObj->setHovering(true);
            for (const auto &obj:m_Display.m_SceneObjects[Display::LayerObjects])
                if (!m_GameLogic->enoughMoney(m_ObjHolding->m_Object)
                    || obj->collides(*m_ObjHolding->m_HoldingSceneObj)) {
                    m_ObjHolding->m_HoldingSceneObj->setHovering(false);
                    break;
                }
            m_Display.drawTempObject(m_ObjHolding->m_HoldingSceneObj);
            break;
        }
        case ObjHoldingSemantics::HoldingAction::HoldPlace: {
            if (m_GameLogic->enoughMoney(m_ObjHolding->m_Object) &&
                m_Display.placeNewBuilding(m_Display.m_CameraPosX - m_ObjHolding->m_PosX,
                                           m_Display.m_CameraPosY - m_ObjHolding->m_PosY, m_ObjHolding->m_Object)) {
                m_GameLogic->buildingAdded(m_ObjHolding->m_Object);
            }
        }
        case ObjHoldingSemantics::HoldingAction::HoldRelease: {
            m_Display.switchClickability(false);
            delete m_ObjHolding;
            m_ObjHolding = nullptr;
            break;
        }
        case ObjHoldingSemantics::HoldingAction::HoldUninitialized: break;
    }
}

void MainLoop::cameraMove(const SDL_Keycode &direction) {
    int32_t &cameraPosX = m_Display.m_CameraPosX, &cameraPosY = m_Display.m_CameraPosY;
    bool moved = false;
    switch (direction) {
        case SDLK_RIGHT:
        case SDLK_d:
            if (cameraPosX + Parameters::getInstance().resX() + Parameters::getInstance().cameraSens() <=
                Parameters::getInstance().mapW()) {
                cameraPosX += Parameters::getInstance().cameraSens();
                moved = true;
            } else if (cameraPosX != Parameters::getInstance().mapW() - Parameters::getInstance().resX()) {
                cameraPosX = Parameters::getInstance().mapW() - Parameters::getInstance().resX();
                moved = true;
            }
            break;
        case SDLK_LEFT:
        case SDLK_a:
            if (cameraPosX - Parameters::getInstance().cameraSens() > 0) {
                cameraPosX -= Parameters::getInstance().cameraSens();
                moved = true;
            } else if (cameraPosX != 0) {
                cameraPosX = 0;
                moved = true;
            }
            break;
        case SDLK_UP:
        case SDLK_w:
            if (cameraPosY - Parameters::getInstance().cameraSens() > 0) {
                cameraPosY -= Parameters::getInstance().cameraSens();
                moved = true;
            } else if (cameraPosY != 0) {
                cameraPosY = 0;
                moved = true;
            }
            break;
        case SDLK_DOWN:
        case SDLK_s:
            if (cameraPosY + Parameters::getInstance().resY() + Parameters::getInstance().cameraSens() <=
                Parameters::getInstance().mapH()) {
                cameraPosY += Parameters::getInstance().cameraSens();
                moved = true;
            } else if (cameraPosY != Parameters::getInstance().mapH() - Parameters::getInstance().resY()) {
                cameraPosY = Parameters::getInstance().mapH() - Parameters::getInstance().resY();
                moved = true;
            }
            break;
        default: break;
    }
    if (moved) {
        m_Display.clearHovered();
        m_Display.cleanDisplay();
        m_Display.drawSceneObjects(Display::LayerBackground);
    }
}

void MainLoop::initializeMenu() {
    m_Display.addMenuObjToScene();
    m_GameOver = true;
    if (m_GameLogic) delete m_GameLogic;
    m_GameLogic = nullptr;
    if (m_ObjHolding) delete m_ObjHolding;
    m_ObjHolding = nullptr;
}


CallbackState MainLoop::displayMessageBox(const std::string &message, CallbackState callback,
                                          const std::string &button) {
    SDL_MessageBoxButtonData* cbButton = new SDL_MessageBoxButtonData();
    cbButton->buttonid = (int32_t) callback;
    cbButton->flags = SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT | SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT;
    cbButton->text = button.c_str();
    SDL_MessageBoxData messageBox;
    messageBox.window = m_Display.m_Window;
    messageBox.title = "";
    messageBox.message = message.c_str();
    messageBox.numbuttons = 1;
    messageBox.buttons = cbButton;
    messageBox.colorScheme = nullptr;
    int buttonNumber;
    SDL_ShowMessageBox(&messageBox, &buttonNumber);
    delete cbButton;
    CallbackState make = (CallbackState) buttonNumber;
    return make;
}


CallbackState MainLoop::displayMessageBox(const std::string &message, CallbackState callback1,
                                          const std::string &button1,
                                          CallbackState callback2, const std::string &button2) {
    SDL_MessageBoxButtonData* cbButton = new SDL_MessageBoxButtonData[2];
    cbButton[0].buttonid = (int32_t) callback1;
    cbButton[1].buttonid = (int32_t) callback2;
    cbButton[0].flags = SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT;
    cbButton[1].flags = SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT;
    cbButton[0].text = button1.c_str();
    cbButton[1].text = button2.c_str();
    SDL_MessageBoxData messageBox;
    messageBox.window = m_Display.m_Window;
    messageBox.title = "";
    messageBox.message = message.c_str();
    messageBox.numbuttons = 2;
    messageBox.buttons = cbButton;
    messageBox.colorScheme = nullptr;
    int buttonNumber;
    SDL_ShowMessageBox(&messageBox, &buttonNumber);
    delete[] cbButton;
    CallbackState make = (CallbackState) buttonNumber;
    return make;
}

Drawable* MainLoop::findOutObjectClicked(int32_t x, int32_t y, int8_t &layer) {
    for (layer = Display::LayerInterface; layer > Display::LayerBackground; --layer)
        for (const auto &obj:m_Display.m_SceneObjects[layer])
            if (obj->isClickable() && obj->inSetOf(x, y)) {
                return obj;
            }
    return nullptr;
}

void MainLoop::initializeNewGame() {
    m_GameOver = false;
    Parameters::getInstance().switchLoadedParams(false);
    if (m_GameLogic) delete m_GameLogic;
    m_GameLogic = nullptr;
    m_GameLogic = new GameLogic(m_GameOver);
    m_Display.addGameObjToScene(m_GameLogic);
}


bool MainLoop::loadGame() {
    std::ifstream saveFile(g_Save, std::ios::in | std::ios::binary | std::ios::ate);
    int64_t fileSize = saveFile.tellg();
    saveFile.seekg(0);
    //buildingsCnt * (3 * sizeof(int)) due to every building is 3 values
    uint64_t buildingsInSaveCnt = fileSize - (sizeof(int32_t) * 5 * g_BuildingsCount + sizeof(uint32_t) * 3) -
                                  (sizeof(std::atomic_long) * 7);
    if (buildingsInSaveCnt % (3 * sizeof(int)) != 0 || !saveFile.is_open() || !saveFile.good() || saveFile.fail()) {
        displayMessageBox("Error opening " + g_Save + " for load save.", CallbackState::CbNone, "Ok.");
        saveFile.close();
        return false;
    }
    m_GameOver = false;
    Parameters::getInstance().switchLoadedParams(true);
    buildingsInSaveCnt /= (3 * sizeof(int));
    char* gameParameters = new char[sizeof(int32_t) * 5 * g_BuildingsCount + sizeof(uint32_t) * 3];
    char* gameResults = new char[sizeof(std::atomic_long) * 7];
    saveFile.read(gameParameters, sizeof(int32_t) * 5 * g_BuildingsCount + sizeof(uint32_t) * 3);
    saveFile.read(gameResults, sizeof(std::atomic_long) * 7);
    Parameters::getInstance().readGameAndBuildingsParamsFromSave(gameParameters);
    if (m_GameLogic) delete m_GameLogic;
    m_GameLogic = nullptr;
    m_GameLogic = new GameLogic(m_GameOver, gameResults);
    m_Display.addGameObjToScene(m_GameLogic);
    delete[] gameParameters;
    delete[] gameResults;
    char* building = new char[3 * sizeof(int)];
    int x, y, callback;
    for (uint64_t counter = 0; counter < buildingsInSaveCnt; counter++) {
        int i = 0;
        saveFile.read(building, 3 * sizeof(int));
        memcpy(&x, building + sizeof(int) * i++, sizeof(int));
        memcpy(&y, building + sizeof(int) * i++, sizeof(int));
        memcpy(&callback, building + sizeof(int) * i, sizeof(int));
        m_Display.placeNewBuilding(x, y, (ObjectType) callback);
    }
    delete[] building;
    return true;
}

void MainLoop::gameSave() {
    std::ofstream saveFile(g_Save, std::ios::out | std::ios::binary);
    if (!saveFile.is_open() || !saveFile.good() || saveFile.fail()) {
        displayMessageBox("Error opening " + g_Save + " for save write.", CallbackState::CbNone, "Ok.");
        saveFile.close();
        return;
    }
    void* gameParameters = Parameters::getInstance().serialize();
    void* gameResults = m_GameLogic->serialize();
    //serialize parameters returns array with size of 5 parameters of int32_t gor each building, plus 3 parameters - map width and height + environment
    saveFile.write((char*) gameParameters, sizeof(int32_t) * 5 * g_BuildingsCount + sizeof(uint32_t) * 3);
    //serialize GameLogic returns array with size of 7 values of atomic longs
    saveFile.write((char*) gameResults, sizeof(std::atomic_long) * 7);
    delete[] (char*) gameParameters;
    delete[] (char*) gameResults;
    m_Display.saveBuildingsToFile(saveFile);
    saveFile.close();
    displayMessageBox("Save done.", CallbackState::CbNone, "Ok.");
}
