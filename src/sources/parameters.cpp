#include "../headers/parameters.h"


void Parameters::fillMap(const std::string &paramPath, std::map<std::string, std::vector<std::string>> &parameters) {
    std::ifstream paramStream;
    checkAndRecoverConfig(paramStream, paramPath);
    std::string line, kind = "";
    bool gotAnything = false;
    bool strict = parameters.empty();
    while (getline(paramStream, line)) {
        if (line[0] == '#') continue;
        if (line[0] == '[' && line.find(']') != std::string::npos) {
            kind = line;
            continue;
        }
        if (kind == "")
            throw ConfigFileFormatException(paramPath, "Each category of config file must start as ['Category']");
        parameters[kind].push_back(line);
        gotAnything = true;
    }
    if (!gotAnything && strict) throw ConfigFileFormatException(paramPath);
    paramStream.close();
}

Parameters::~Parameters() {
    for (auto &a:m_BuildingParameters)
        if (a) {
            delete a;
            a = nullptr;
        }
    for (auto &a:m_BuildingParamsSave)
        if (a) {
            delete a;
            a = nullptr;
        }
}

void Parameters::readConfig() {
    std::map<std::string, std::vector<std::string>> parameters;
    fillMap(g_ConfigDefault, parameters);
    fillMap(g_Config, parameters);
    const std::string graphics = "[Graphics]";
    const std::string buildings = "[Buildings]";
    const std::string game = "[GameLogic]";
    auto iterGr = parameters.find(graphics);
    if (iterGr != parameters.end()) {
        parseGraphics(iterGr->second);
        parameters.erase(iterGr);
    } else throw ConfigFileFormatException("Config", graphics + " category not found");
    auto iterGame = parameters.find(game);
    if (iterGame != parameters.end()) {
        parseGame(iterGame->second);
        parameters.erase(iterGame);
    } else throw ConfigFileFormatException("Config", game + " category not found");
    auto iterBuilding = parameters.find(buildings);
    if (iterBuilding != parameters.end()) {
        parseBuildings(iterBuilding->second);
        parameters.erase(iterBuilding);
    } else throw ConfigFileFormatException("Config", buildings + " category not found");
    m_AllInit = true;
}


void Parameters::checkAndRecoverConfig(std::ifstream &paramStream, const std::string &paramPath) const {
    paramStream.open(paramPath, std::ios::in);
    if (!paramStream.is_open() || !paramStream.good() || paramStream.fail()) {
        paramStream.close();
        if (g_ConfigDefault == paramPath) throw FileOpenException("with default config");
        std::ifstream src(g_ConfigDefault, std::ios::binary);
        std::ofstream dst(paramPath, std::ios::binary);
        dst << src.rdbuf();
        src.close();
        dst.close();
        paramStream.open(paramPath, std::ios::in);
        if (!paramStream.is_open() || !paramStream.good() || paramStream.fail())
            throw FileOpenException(paramPath);
    }
}


void Parameters::parseGame(const std::vector<std::string> &parameters) {
    const std::string mapSizeX = "MapSizeX";
    const std::string mapSizeY = "MapSizeY";
    const std::string env = "Environment";
    const std::string stMoney = "StartMoney";
    for (const auto &consideredStr:parameters) {
        try {
            if (!consideredStr.find(mapSizeX)) {
                int mapW = std::stoi(consideredStr.substr(mapSizeX.size() + 1, consideredStr.size()));
                if ((uint32_t) mapW < m_ResX || mapW > 20000)
                    throw ConfigFileFormatException(
                            "Config", consideredStr +
                                      "\nMapSize can be as big as resolution or more, max is 20000x20000.");
                m_MapW = (uint32_t) mapW;
                continue;
            } else if (!consideredStr.find(mapSizeY)) {
                int mapH = std::stoi(consideredStr.substr(mapSizeY.size() + 1, consideredStr.size()));
                if ((uint32_t) mapH < m_ResY || mapH > 20000)
                    throw ConfigFileFormatException(
                            "Config", consideredStr +
                                      "\nMapSize can be as big as resolution or more, max is 20000x20000.");
                m_MapH = (uint32_t) mapH;
                continue;
            } else if (!consideredStr.find(env)) {
                int envN = std::stoi(consideredStr.substr(env.size() + 1, consideredStr.size()));
                if (envN < 1 || envN > 5)
                    throw ConfigFileFormatException(
                            "Config", consideredStr +
                                      "\nEnvironment can be 1 to 5 (desert, dry, urban, prolific, blank).");
                m_Env = (uint8_t) envN;
                continue;
            } else if (!consideredStr.find(stMoney)) {
                int money = std::stoi(consideredStr.substr(stMoney.size() + 1, consideredStr.size()));
                if (money <= 0) throw ConfigFileFormatException("Config", consideredStr);
                m_InitMoney = money;
                continue;
            }
            throw ConfigFileFormatException("Config", consideredStr);
        } catch (std::invalid_argument &ex) {
            throw ConfigFileFormatException("Config", consideredStr);
        }
    }
}


void Parameters::parseBuildings(const std::vector<std::string> &buildings) {
    const std::vector<std::string> buildingsNames = {
            "-Apartments-", "-Restaurant-", "-Pub-", "-Supermarket-",
            "-Police-", "-Hospital-", "-Brothel-", "-Skyscraper-", "-Bank-"};
    const std::vector<std::string> parameters = {"Money/s", "Happiness/s", "Crime/s", "People", "Cost"};
    int32_t tempParametersValues[5];
    m_BuildingParameters.resize(buildingsNames.size(), nullptr);
    //int32_t moneyPs, happyPs, crimePs, givePeople, cost;
    for (const auto &consideredStr: buildings) {
        uint32_t buildingNum = 0;
        for (const auto &searchBuilding: buildingsNames) {
            if (!consideredStr.compare(0, searchBuilding.length(), searchBuilding)) break;
            buildingNum++;
        }
        if (buildingNum == buildingsNames.size())
            throw ConfigFileFormatException(
                    "Config", "\n" + consideredStr + "\nUnknown building name.");
        int i = 0;
        for (const auto &param:parameters) {
            size_t found = consideredStr.find(param);
            if (found == std::string::npos)
                throw ConfigFileFormatException(
                        "Config", "\n" + consideredStr + "\nParameter " + param + " not found.");
            std::string fullParameter = consideredStr.substr(
                    found, consideredStr.substr(found, std::string::npos).find_first_of(','));
            try {
                tempParametersValues[i++] = std::stoi(fullParameter.substr(param.size() + 1, std::string::npos));
            } catch (std::invalid_argument &ex) {
                throw ConfigFileFormatException(
                        "Config", "\n" + consideredStr + "\nParameter value syntax error.");
            }
        }
        if (m_BuildingParameters[buildingNum]) delete m_BuildingParameters[buildingNum];
        m_BuildingParameters[buildingNum] = new BuildingParameters(
                tempParametersValues[0], tempParametersValues[1], tempParametersValues[2],
                tempParametersValues[3], tempParametersValues[4]);
    }
}


void Parameters::parseGraphics(const std::vector<std::string> &parameters) {
    const std::string resX = "ResX";
    const std::string resY = "ResY";
    const std::string screen = "FullScreen";
    const std::string cameraSens = "CameraSensitivity";
    for (const auto &consideredStr:parameters) {
        try {
            if (!consideredStr.find(resX)) {
                int32_t resXtmp = std::stoi(consideredStr.substr(resX.size() + 1, consideredStr.size()));
                if (resXtmp < 800)
                    throw ConfigFileFormatException(
                            "Config", consideredStr +
                                      "\nResolution recommended 800x600 or 1024x768, cannot be less than 800x600");
                m_ResX = (uint32_t) resXtmp;
                continue;
            } else if (!consideredStr.find(resY)) {
                int32_t resYtmp = std::stoi(consideredStr.substr(resY.size() + 1, consideredStr.size()));
                if (resYtmp < 600)
                    throw ConfigFileFormatException(
                            "Config", consideredStr +
                                      "\nResolution recommended 800x600 or 1024x768, cannot be less than 800x600");
                m_ResY = (uint32_t) resYtmp;
                continue;
            } else if (!consideredStr.find(screen)) {
                m_Fullscreen = (bool) std::stoi(consideredStr.substr(screen.size() + 1, consideredStr.size()));
                if (m_Fullscreen != 0 && m_Fullscreen != 1)
                    throw ConfigFileFormatException(
                            "Config", consideredStr + "\nFullscreen must be 0 or 1 (off or on)");
                continue;
            } else if (!consideredStr.find(cameraSens)) {
                int32_t sensTmp = std::stoi(consideredStr.substr(cameraSens.size() + 1, consideredStr.size()));
                if (sensTmp <= 0 || sensTmp > 100) throw ConfigFileFormatException("Config", consideredStr);
                m_Sens = (uint32_t) 5 * sensTmp;
                continue;
            }
            throw ConfigFileFormatException("Config", consideredStr);
        }
        catch (std::invalid_argument &ex) {
            throw ConfigFileFormatException("Config", consideredStr);
        }
    }
}

void Parameters::setTexturesSizes(uint32_t small, uint32_t medium, uint32_t big) {
    m_TextureSmall = small;
    m_TextureMedium = medium;
    m_TextureBig = big;
}

Parameters &Parameters::getInstance() {
    static Parameters instance;
    return instance;
}

uint32_t Parameters::getEnvironmentFirst() const {
    uint32_t env;
    env = m_LoadedGame ? m_EnvSave : m_Env;
    switch (env) {
        case 1: return SpriteEnvSand;
        case 4:
        case 3: return SpriteEnvGrass;
        default: return SpriteEnvGround;
    }
}

uint32_t Parameters::getEnvironmentSecond() const {
    uint32_t env;
    env = m_LoadedGame ? m_EnvSave : m_Env;
    switch (env) {
        case 1:
        case 2: return SpriteEnvSand;
        case 4: return SpriteEnvGrass;
        default: return SpriteEnvGround;
    }
}

void* Parameters::serialize() {
    //5 parameters in each building (9) plus 3 global parameters MapH, MapW and environment
    char* serialized = new char[sizeof(int32_t) * 5 * g_BuildingsCount + sizeof(uint32_t) * 3];
    int i = 0;
    memcpy(serialized + sizeof(uint32_t) * i++, &m_MapW, sizeof(uint32_t));
    memcpy(serialized + sizeof(uint32_t) * i++, &m_MapH, sizeof(uint32_t));
    memcpy(serialized + sizeof(uint32_t) * i++, &m_Env, sizeof(uint32_t));
    for (const auto &a:m_BuildingParameters) {
        void* bldParams = a->serialize();
        memcpy(serialized + sizeof(int32_t) * i, bldParams, sizeof(int32_t) * 5);
        i += 5;
        delete[] (char*) bldParams;
    }
    return serialized;
}

void Parameters::readGameAndBuildingsParamsFromSave(const void* serialized) {
    int i = 0;
    memcpy(&m_MapWSave, (char*) serialized + sizeof(uint32_t) * i++, sizeof(uint32_t));
    memcpy(&m_MapHSave, (char*) serialized + sizeof(uint32_t) * i++, sizeof(uint32_t));
    memcpy(&m_EnvSave, (char*) serialized + sizeof(uint32_t) * i++, sizeof(uint32_t));
    for (auto &a:m_BuildingParamsSave) delete a;
    m_BuildingParamsSave.clear();
    for (int j = 0; j < g_BuildingsCount; j++) {
        m_BuildingParamsSave.push_back(new BuildingParameters((char*) serialized + sizeof(uint32_t) * i));
        i += 5;
    }
}


void* Parameters::BuildingParameters::serialize() const {
    char* serialized = new char[sizeof(int32_t) * 5];
    memcpy(serialized + sizeof(int32_t) * 0, &m_MoneyPs, sizeof(int32_t));
    memcpy(serialized + sizeof(int32_t) * 1, &m_HappyPs, sizeof(int32_t));
    memcpy(serialized + sizeof(int32_t) * 2, &m_CrimePs, sizeof(int32_t));
    memcpy(serialized + sizeof(int32_t) * 3, &m_GivePeople, sizeof(int32_t));
    memcpy(serialized + sizeof(int32_t) * 4, &m_Cost, sizeof(int32_t));
    return serialized;
}

Parameters::BuildingParameters::BuildingParameters(const void* serialized) :
        m_MoneyPs(parseInt(serialized, 0)), m_HappyPs(parseInt(serialized, 1)),
        m_CrimePs(parseInt(serialized, 2)), m_GivePeople(parseInt(serialized, 3)), m_Cost(parseInt(serialized, 4)) {

}

int32_t Parameters::BuildingParameters::parseInt(const void* serialized, int32_t ind) {
    int32_t ret;
    memcpy(&ret, (char*) serialized + sizeof(int32_t) * ind, sizeof(int32_t));
    return ret;
}
