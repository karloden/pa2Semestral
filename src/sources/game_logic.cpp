#include "../headers/game_logic.h"

void ObjHoldingSemantics::setHoldingSceneObj(Drawable* scObj, int32_t size) {
    m_HoldingSceneObj = scObj;
    m_Size = size;
}


GameLogic::GameLogic(std::atomic_bool &gameOverFlag) :
        m_BuildingParameters(Parameters::getInstance().getBuildingParameters()),
        m_CounterThread(&GameLogic::valuesCounter, this, std::ref(gameOverFlag)),
        m_Money(Parameters::getInstance().getInitMoney()), m_MoneyPS(0), m_Happiness(0), m_HappinessPS(0),
        m_Crime(0), m_CrimePS(0), m_People(0) {}

void GameLogic::valuesCounter(std::atomic_bool &gameOverFlag) {
    while (!gameOverFlag) {
        SDL_Delay(1000);
        std::unique_lock<std::mutex> lock(m_ChangerMutex);
        m_Crime += m_CrimePS;
        m_Happiness += m_HappinessPS;
        m_Money += m_MoneyPS;
        m_CrimePS += m_People / 1000;
        m_HappinessPS -= m_Crime / 1000;
        m_Money += (m_Happiness - m_Crime) / 100;
        m_People += (m_Happiness - m_Crime) / 100;
        while (m_Crime > 100000 && m_Happiness > 100000) {
            m_Crime -= 100000;
            m_Happiness -= 100000;
        }
        lock.unlock();
        if (m_People < 0) {
            m_People = 0;
            gameOverFlag = true;
            return;
        }
    }
}

void GameLogic::buildingAdded(ObjectType building) {
    std::unique_lock<std::mutex> lock(m_ChangerMutex);
    m_MoneyPS += m_BuildingParameters[building]->m_MoneyPs;
    m_HappinessPS += m_BuildingParameters[building]->m_HappyPs;
    m_CrimePS += m_BuildingParameters[building]->m_CrimePs;
    m_People += m_BuildingParameters[building]->m_GivePeople;
    m_Money -= m_BuildingParameters[building]->m_Cost;
    lock.unlock();
}

void GameLogic::buildingRemoved(ObjectType building) {
    std::unique_lock<std::mutex> lock(m_ChangerMutex);
    m_MoneyPS -= m_BuildingParameters[building]->m_MoneyPs;
    m_HappinessPS -= m_BuildingParameters[building]->m_HappyPs;
    m_CrimePS -= m_BuildingParameters[building]->m_CrimePs;
    m_People -= m_BuildingParameters[building]->m_GivePeople;
    m_Money += m_BuildingParameters[building]->m_Cost / 4;
    lock.unlock();
}


GameLogic::GameLogic(std::atomic_bool &gameOverFlag, const void* serialized) :
        m_BuildingParameters(Parameters::getInstance().getBuildingParameters()),
        m_CounterThread(&GameLogic::valuesCounter, this, std::ref(gameOverFlag)) {
    int i = 0;
    memcpy(&m_Money, (char*) serialized + sizeof(std::atomic_long) * i++, sizeof(std::atomic_long));
    memcpy(&m_MoneyPS, (char*) serialized + sizeof(std::atomic_long) * i++, sizeof(std::atomic_long));
    memcpy(&m_Happiness, (char*) serialized + sizeof(std::atomic_long) * i++, sizeof(std::atomic_long));
    memcpy(&m_HappinessPS, (char*) serialized + sizeof(std::atomic_long) * i++, sizeof(std::atomic_long));
    memcpy(&m_Crime, (char*) serialized + sizeof(std::atomic_long) * i++, sizeof(std::atomic_long));
    memcpy(&m_CrimePS, (char*) serialized + sizeof(std::atomic_long) * i++, sizeof(std::atomic_long));
    memcpy(&m_People, (char*) serialized + sizeof(std::atomic_long) * i, sizeof(std::atomic_long));
}


void* GameLogic::serialize() {
    char* serialized = new char[sizeof(std::atomic_long) * 7];
    int i = 0;
    memcpy(serialized + sizeof(std::atomic_long) * i++, &m_Money, sizeof(std::atomic_long));
    memcpy(serialized + sizeof(std::atomic_long) * i++, &m_MoneyPS, sizeof(std::atomic_long));
    memcpy(serialized + sizeof(std::atomic_long) * i++, &m_Happiness, sizeof(std::atomic_long));
    memcpy(serialized + sizeof(std::atomic_long) * i++, &m_HappinessPS, sizeof(std::atomic_long));
    memcpy(serialized + sizeof(std::atomic_long) * i++, &m_Crime, sizeof(std::atomic_long));
    memcpy(serialized + sizeof(std::atomic_long) * i++, &m_CrimePS, sizeof(std::atomic_long));
    memcpy(serialized + sizeof(std::atomic_long) * i, &m_People, sizeof(std::atomic_long));
    return serialized;
}
