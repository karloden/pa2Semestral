#include "../headers/drawable.h"

SDL_Renderer* Drawable::m_Renderer;

Drawable::Drawable(uint32_t x, uint32_t y, uint32_t w, uint32_t h, int32_t* relativePosX, int32_t* relativePosY,
                   bool clickable, CallbackState callback) :
        m_XRelative(relativePosX), m_YRelative(relativePosY), m_Callback(callback),
        m_Clickable(clickable), m_Hovered(false), m_DefClickable(m_Clickable),
        m_HoverLocked(false) {
    m_Pos.w = w;
    m_Pos.h = h;
    m_Pos.x = x;
    m_Pos.y = y;
}

bool Drawable::inSetOf(int32_t x, int32_t y) const {
    return ((m_XRelative ? x + *m_XRelative > m_Pos.x : x > m_Pos.x) &&
            (m_XRelative ? x + *m_XRelative < m_Pos.x + m_Pos.w : x < m_Pos.x + m_Pos.w) &&
            (m_YRelative ? y + *m_YRelative > m_Pos.y : y > m_Pos.y) &&
            (m_YRelative ? y + *m_YRelative < m_Pos.y + m_Pos.h : y < m_Pos.y + m_Pos.h));
}

void Drawable::disableClickable(bool disabled) {
    if (disabled) m_Clickable = false;
    else m_Clickable = m_DefClickable;
}

bool Drawable::collides(const Drawable &other) const {
    return ((m_YRelative ? m_Pos.y + m_Pos.h - *m_YRelative : m_Pos.y + m_Pos.h) >
            (other.m_YRelative ? other.m_Pos.y - *(other.m_YRelative) : other.m_Pos.y) &&

            (m_YRelative ? m_Pos.y - *m_YRelative : m_Pos.y) <
            (other.m_YRelative ? other.m_Pos.y + other.m_Pos.h - *(other.m_YRelative) :
             other.m_Pos.y + other.m_Pos.h) &&

            (m_XRelative ? m_Pos.x + m_Pos.w - *m_XRelative : m_Pos.x + m_Pos.w) >
            (other.m_XRelative ? other.m_Pos.x - *(other.m_XRelative) : other.m_Pos.x) &&

            (m_XRelative ? m_Pos.x - *m_XRelative : m_Pos.x) <
            (other.m_XRelative ? other.m_Pos.x + other.m_Pos.w - *(other.m_XRelative) :
             other.m_Pos.x + other.m_Pos.w));
}

void Drawable::hoveringToSelected(bool enabled) {
    m_HoverLocked = false;
    setHovering(enabled);
    m_HoverLocked = enabled;
}

void* Drawable::serialize() {
    //mpos, callback
    int i = 0;
    char* serialized = new char[3 * sizeof(int)];
    memcpy(serialized + sizeof(int) * i++, &m_Pos.x, sizeof(int));
    memcpy(serialized + sizeof(int) * i++, &m_Pos.y, sizeof(int));
    memcpy(serialized + sizeof(int) * i, &m_Callback, sizeof(int));
    return serialized;
}


TextButton::TextButton(uint32_t x, uint32_t y, uint32_t fontSize, const std::string &text, const SDL_Color &color,
                       CallbackState callback, Sprite* buttonSprite) :
        TextInfo(x, y, fontSize, text, color, nullptr, nullptr, false) {
    m_DefClickable = true;
    m_Callback = callback;
    m_Clickable = true;
    SDL_Texture* textRaw = m_CookedText;

    m_CookedTextHovered = SDL_CreateTexture(m_Renderer, SDL_PIXELFORMAT_RGBA8888,
                                            SDL_TEXTUREACCESS_TARGET, m_Pos.w, m_Pos.h);
    m_CookedText = SDL_CreateTexture(m_Renderer, SDL_PIXELFORMAT_RGBA8888,
                                     SDL_TEXTUREACCESS_TARGET, m_Pos.w, m_Pos.h);
    SDL_SetRenderTarget(m_Renderer, m_CookedTextHovered);
    SDL_SetRenderDrawColor(m_Renderer, 85, 114, 38, 0);
    SDL_RenderClear(m_Renderer);
    SDL_RenderCopy(m_Renderer, textRaw, nullptr, nullptr);
    SDL_SetRenderTarget(m_Renderer, m_CookedText);
    if (!buttonSprite) {
        SDL_SetRenderDrawColor(m_Renderer, 0, 0, 0, 0);
        SDL_RenderClear(m_Renderer);
    } else {
        int buttonRes = buttonSprite->getRes() / 3;
        SDL_Rect a;
        a.w = buttonRes;
        a.h = buttonRes / 2;
        a.x = 0;
        a.y = buttonRes / 4;
        SDL_RenderCopy(m_Renderer, buttonSprite->getTexture(false), &a, nullptr);
    }
    SDL_RenderCopy(m_Renderer, textRaw, nullptr, nullptr);
    SDL_SetRenderTarget(m_Renderer, nullptr);
    SDL_DestroyTexture(textRaw);
}


TextInfo::TextInfo(uint32_t x, uint32_t y, uint32_t fontSize, const std::string &text, const SDL_Color &color,
                   int32_t* relativePosX, int32_t* relativePosY, bool ital)
        : Drawable(x, y, (uint32_t) ((fontSize * text.size()) / 2.25), fontSize, relativePosX, relativePosY, false,
                   CallbackState::CbReleaseSelected) {

    TTF_Font* font;
    if (!ital) font = TTF_OpenFont(g_MainFontPath.c_str(), fontSize);
    else font = TTF_OpenFont(g_MainItalFontPath.c_str(), fontSize);
    SDL_Surface* surface = TTF_RenderText_Solid(font, text.c_str(), color);
    m_CookedText = SDL_CreateTextureFromSurface(m_Renderer, surface);
    SDL_FreeSurface(surface);
    TTF_CloseFont(font);
}


void StaticTextInfo::draw() const {
    if (m_Value) {
        SDL_Rect rend = m_Pos;
        uint8_t digits = (uint8_t) (getNumberOfDigits(*m_Value) + 2);
        rend.x += m_Pos.w;
        rend.w = (uint32_t) ((m_Pos.h * digits) / 2.25);
        std::string valueText(": " + std::to_string(*m_Value));
        SDL_Surface* surface = TTF_RenderText_Solid(m_Font, valueText.c_str(), m_Color);
        SDL_Texture* valueTexture = SDL_CreateTextureFromSurface(m_Renderer, surface);
        SDL_RenderCopy(m_Renderer, valueTexture, nullptr, &rend);
        SDL_FreeSurface(surface);
        SDL_DestroyTexture(valueTexture);
    }
    SDL_RenderCopy(m_Renderer, m_CookedText, nullptr, &m_Pos);
}


void TextInfo::draw() const {
    if ((m_XRelative ? (m_Pos.x - *m_XRelative) + m_Pos.w >= 0 : m_Pos.x + m_Pos.w >= 0) &&
        (m_XRelative ? (m_Pos.x - *m_XRelative) < Parameters::getInstance().resX() :
         m_Pos.x < Parameters::getInstance().resX()) &&

        (m_YRelative ? (m_Pos.y - *m_YRelative) + m_Pos.h >= 0 : m_Pos.y + m_Pos.h >= 0) &&
        (m_YRelative ? (m_Pos.y - *m_YRelative) < Parameters::getInstance().resY() :
         m_Pos.y < Parameters::getInstance().resY())) {
        SDL_Rect rend;
        if (m_XRelative)rend.x = m_Pos.x - *m_XRelative; else rend.x = m_Pos.x;
        if (m_YRelative)rend.y = m_Pos.y - *m_YRelative; else rend.y = m_Pos.y;
        rend.w = m_Pos.w;
        rend.h = m_Pos.h;
        SDL_RenderCopy(m_Renderer, m_CookedText, nullptr, &rend);
    }
}


uint8_t StaticTextInfo::getNumberOfDigits(int64_t number) const {
    int8_t sign = number < 0 ? 1 : 0;
    return (uint8_t) (abs(number) > 0 ? (int) log10((double) abs(number)) + 1 + sign : 1 + sign);
}

StaticTextInfo::StaticTextInfo(uint32_t x, uint32_t y, uint32_t fontSize, const std::string &text,
                               const SDL_Color &color,
                               const std::atomic_long* printValue, bool ital) :
        TextInfo(x, y, fontSize, text, color, nullptr, nullptr, ital), m_Value(printValue),
        m_Color(color) {
    if (!ital) m_Font = TTF_OpenFont(g_MainFontPath.c_str(), fontSize);
    else m_Font = TTF_OpenFont(g_MainItalFontPath.c_str(), fontSize);
}


void TextButton::setHovering(bool hovered) {
    if (!m_HoverLocked && hovered != m_Hovered) {
        SDL_Texture* tmp = m_CookedTextHovered;
        m_CookedTextHovered = m_CookedText;
        m_CookedText = tmp;
        m_Hovered = hovered;
    }
}


void BuildingDisplayObject::setHovering(bool hovered) {
    if (!m_HoverLocked && hovered != m_Hovered) m_Hovered = hovered;
}

BuildingDisplayObject::BuildingDisplayObject(uint32_t x, uint32_t y, int32_t* relativePosX, int32_t* relativePosY,
                                             Sprite* sprite,
                                             ObjectType callback, uint32_t size) :
        StaticObject(x, y, relativePosX, relativePosY, sprite, size) {
    m_Callback = (CallbackState) callback;
    m_Clickable = true;
    m_DefClickable = true;
}

StaticObject::StaticObject(uint32_t x, uint32_t y, int32_t* relativePosX, int32_t* relativePosY, Sprite* sprite,
                           uint32_t size)
        :
        Drawable(x, y, size ? size : (uint32_t) sprite->getRes(), size ? size : (uint32_t) sprite->getRes(),
                 relativePosX, relativePosY, false, CallbackState::CbReleaseSelected),
        m_Sprite(sprite) {}


void StaticObject::draw() const {
    if ((m_XRelative ? (m_Pos.x - *m_XRelative) + m_Pos.w >= 0 : m_Pos.x + m_Pos.w >= 0) &&
        (m_XRelative ? (m_Pos.x - *m_XRelative) < Parameters::getInstance().resX() :
         m_Pos.x < Parameters::getInstance().resX()) &&
        (m_YRelative ? (m_Pos.y - *m_YRelative) + m_Pos.h >= 0 : m_Pos.y + m_Pos.h >= 0) &&
        (m_YRelative ? (m_Pos.y - *m_YRelative) < Parameters::getInstance().resY() :
         m_Pos.y < Parameters::getInstance().resY())) {
        SDL_Rect rend;
        if (m_XRelative) rend.x = m_Pos.x - *m_XRelative; else rend.x = m_Pos.x;
        if (m_YRelative) rend.y = m_Pos.y - *m_YRelative; else rend.y = m_Pos.y;
        rend.w = m_Pos.w;
        rend.h = m_Pos.h;
        SDL_RenderCopy(m_Renderer, m_Sprite->getTexture(m_Hovered), nullptr, &rend);
    }
}
