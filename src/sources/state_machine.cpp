#include "../headers/state_machine.h"


StateMachine::StateMachine(MainLoop* injectedMainLoop) :
        m_MainLoop(injectedMainLoop), m_CurrentState(CallbackState::StCbMenu), m_Event(new SDL_Event()),
        m_MouseMoveTicks(0), m_TempObjectDrawingState(false) { newInit(); }

void StateMachine::pollEvent() {
    while (SDL_PollEvent(m_Event)) {
        switch (m_Event->type) {
            case SDL_QUIT:
                if (m_CurrentState == CallbackState::StCbGame)
                    resolveCallbackState(CallbackState::CbMenuFromGame);
                else m_MainLoop->m_IsClosed = true;
                return;
            case SDL_KEYDOWN: keyboardEvent(m_Event);
                break;
            case SDL_MOUSEMOTION: m_MouseMoveTicks++;
                if (m_MouseMoveTicks >= 10) {
                    mouseMoveEvent(m_Event);
                    m_MouseMoveTicks = 0;
                }
                break;
            case SDL_MOUSEBUTTONDOWN: mouseButtonEvent(m_Event);
                break;
            default: break;
        }
    }
    if (m_MainLoop->m_GameOver && m_CurrentState == CallbackState::StCbGame) {
        resolveCallbackState(
                m_MainLoop->displayMessageBox(
                        "Game over! You've lost. Try again?", CallbackState::StCbGame, "Yes.",
                        CallbackState::StCbMenu, "No."));
        return;
    }
    SDL_Delay(10);
    if (!m_TempObjectDrawingState)
        m_MainLoop->reDraw(MainLoop::LayerInterface);
}

void StateMachine::mouseMoveEvent(const SDL_Event* event) {
    if (event->motion.x < 0 || event->motion.y < 0) return;
    if (m_CurrentState == CallbackState::StCbGameHoldObject) {
        m_MainLoop->resolveHolding(
                ObjHoldingSemantics(ObjHoldingSemantics::HoldingAction::HoldMove, event->motion.x, event->motion.y));
        return;
    }
    m_MainLoop->mouseMovedTo(event->motion.x, event->motion.y);
}


void StateMachine::keyboardEvent(const SDL_Event* event) {
    if (event->key.keysym.sym == SDLK_ESCAPE)
        switch (m_CurrentState) {
            case CallbackState::StCbGame:
                if (m_Selected)
                    resolveCallbackState(CallbackState::CbReleaseSelected);
                return;
            case CallbackState::StCbGameHoldObject: m_TempObjectDrawingState = false;
                m_MainLoop->resolveHolding(ObjHoldingSemantics(ObjHoldingSemantics::HoldingAction::HoldRelease));
                m_CurrentState = CallbackState::StCbGame;
                return;
            case CallbackState::StCbMenu: m_MainLoop->m_IsClosed = true;
                return;
            default: return;
        }
    if (m_Selected && event->key.keysym.sym == SDLK_DELETE) {
        m_MainLoop->deleteObject(m_Selected, m_SelectedType);
        m_Selected = nullptr;
        return;
    }
    if (m_CurrentState == CallbackState::StCbGame)
        m_MainLoop->cameraMove(event->key.keysym.sym);
}

void StateMachine::mouseButtonEvent(const SDL_Event* event) {
    if (m_CurrentState == CallbackState::StCbGameHoldObject) {
        if (event->button.button == SDL_BUTTON_RIGHT) {
            m_TempObjectDrawingState = false;
            m_MainLoop->resolveHolding(ObjHoldingSemantics(ObjHoldingSemantics::HoldingAction::HoldRelease));
            m_CurrentState = CallbackState::StCbGame;
            return;
        } else {
            m_TempObjectDrawingState = false;
            m_MainLoop->resolveHolding(ObjHoldingSemantics(ObjHoldingSemantics::HoldingAction::HoldPlace));
            m_CurrentState = CallbackState::StCbGame;
            return;
        }
    }
    int8_t layer = MainLoop::LayerObjects;
    Drawable* objectClicked = m_MainLoop->findOutObjectClicked(event->motion.x, event->motion.y, layer);
    if (!objectClicked) {
        resolveCallbackState(CallbackState::CbReleaseSelected);
        return;
    }
    CallbackState callback = objectClicked->callbackAction();
    if ((ObjectType) callback < g_BuildingsCount && (ObjectType) callback >= 0) {
        buildingChosen(callback, objectClicked, layer == MainLoop::LayerObjects);
        return;
    } else {
        resolveCallbackState(callback);
        return;
    }
}


void StateMachine::buildingChosen(const CallbackState &buildingType, Drawable* context, bool builtAlready) {
    if (m_BuildMenuOpened && !builtAlready) {
        resolveCallbackState(CallbackState::CbReleaseSelected);
        m_TempObjectDrawingState = true;
        m_MainLoop->resolveHolding(
                ObjHoldingSemantics(ObjHoldingSemantics::HoldingAction::HoldInit, (ObjectType) buildingType));
        m_CurrentState = CallbackState::StCbGameHoldObject;
    } else {
        if (m_Selected) m_Selected->hoveringToSelected(false);
        if (m_Selected == context) {
            resolveCallbackState(CallbackState::CbReleaseSelected);
        } else {
            m_Selected = context;
            m_Selected->hoveringToSelected(true);
            m_SelectedType = (ObjectType) buildingType;
        }
        m_MainLoop->reDraw(MainLoop::LayerObjects);
    }
}


void StateMachine::resolveCallbackState(const CallbackState &callback) {
    switch (callback) {
        case CallbackState::StCbMenu: m_MainLoop->initializeMenu();
            m_CurrentState = CallbackState::StCbMenu;
            return;
        case CallbackState::CbMenuFromGame:
            resolveCallbackState(m_MainLoop->displayMessageBox(
                    "Are you sure? All unsaved progress will be lost.", CallbackState::StCbMenu, "Yes.",
                    CallbackState::CbNone, "No."));
            return;
        case CallbackState::StCbGame: newInit();
            m_MainLoop->initializeNewGame();
            m_CurrentState = CallbackState::StCbGame;
            return;
        case CallbackState::CbGameBuildMenu:
            if (m_GameMenuOpened) {
                m_MainLoop->gameMenu(false);
                m_GameMenuOpened = false;
            }
            m_MainLoop->buildMenu(!m_BuildMenuOpened);
            m_BuildMenuOpened = !m_BuildMenuOpened;
            return;
        case CallbackState::CbGameMenu:
            if (m_BuildMenuOpened) {
                m_MainLoop->buildMenu(false);
                m_BuildMenuOpened = false;
            }
            m_MainLoop->gameMenu(!m_GameMenuOpened);
            m_GameMenuOpened = !m_GameMenuOpened;
            return;
        case CallbackState::CbReleaseSelected:
            if (m_Selected) {
                m_Selected->hoveringToSelected(false);
                m_Selected = nullptr;
            }
            return;
        case CallbackState::CbExit: m_MainLoop->m_IsClosed = true;
            m_CurrentState = CallbackState::CbExit;
            return;
        case CallbackState::StCbSaveGame: m_MainLoop->gameSave();
            return;
        case CallbackState::StCbLoadGame: newInit();
            if (m_MainLoop->loadGame())
                m_CurrentState = CallbackState::StCbGame;
            return;
        default: return;
    }
}


void StateMachine::newInit() {
    m_BuildMenuOpened = false;
    m_Selected = nullptr;
    m_GameMenuOpened = false;
}
