#include "../headers/misc.h"

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)

std::string getPathToBinary() {
    char buf[1024] = {0};
    DWORD ret = GetModuleFileNameA(NULL, buf, sizeof(buf));
    if (ret == 0 || ret == sizeof(buf))
    {
        return executable_path_fallback(argv0);
    }
    return buf;
}

#else

std::string getPathToBinary() {
    char result[PATH_MAX];
    ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
    std::string path(result, (unsigned long) ((count > 0) ? count : 0));
    path = path.substr(0, path.find_last_of("/") + 1);
    return path;
}

#endif


std::string convertBackspaces(const std::string &str) {
    std::string retStr = str;
    auto iter = retStr.begin();
    auto end = retStr.end();
    while (iter != end) {
        iter = std::find(iter, end, '\b');
        if (iter == end) break;
        if (iter == retStr.begin())
            iter = retStr.erase(iter);
        else
            iter = retStr.erase(iter - 1, iter + 1);
        end = retStr.end();
    }
    return retStr;
}
