#include "../headers/sprites.h"
#include "../headers/parameters.h"

Sprite::Sprite(const ObjectType &kind, SDL_Texture* texture, SDL_Texture* textureCopy) : m_Kind(kind),
                                                                                         m_Texture(texture),
                                                                                         m_TextureHovered(textureCopy) {
    SDL_SetTextureColorMod(m_TextureHovered, 70, 255, 70);
}

Sprite::~Sprite() {
    SDL_DestroyTexture(m_Texture);
    SDL_DestroyTexture(m_TextureHovered);
}

SDL_Texture* Sprite::getTexture(bool hovered) const {
    if (hovered) return m_TextureHovered;
    else return m_Texture;
}

uint32_t Sprite::getRes() const {
    switch (m_Kind) {
        case BuildingSupermarket:
        case BuildingSkyscraper: return Parameters::getInstance().textureSizeBig() * 3;
        case BuildingApartments:
        case BuildingHospital:
        case BuildingRestaurant:
        case BuildingBank: return Parameters::getInstance().textureSizeMedium() * 3;
        case SpriteIntInterface: return Parameters::getInstance().textureSizeSmall() * 2;
        case SpriteIntLogo: return (uint32_t) (Parameters::getInstance().resY());
        default: return Parameters::getInstance().textureSizeSmall() * 3;
    }
}

