#include <iostream>
#include "headers/misc.h"
#include "headers/state_machine.h"

//Compile time parameters
#ifdef __RELEASE__
const std::string g_PathBinary = getPathToBinary();
#endif
#ifdef __DEBUG__
const std::string g_PathBinary = "";
#endif
const std::string g_WindowName = "FallBuild v1.0";
const std::string g_ConfigDefault = g_PathBinary + "config/baseTEMPLATE.cfg";
const std::string g_Config = g_PathBinary + "config/base.cfg";
const std::string g_Save = g_PathBinary + "config/save";
const std::string g_ResourcesPath = g_PathBinary + "resources/";
const std::string g_SpritesPath = g_ResourcesPath + "sprites.png";
const std::string g_LogoPath = g_ResourcesPath + "logo.png";
const std::string g_MainFontPath = g_ResourcesPath + "font_main.ttf";
const std::string g_MainItalFontPath = g_ResourcesPath + "font_main_ital.ttf";


int main(int argc, char** argv) {
    MainLoop* mainLoop = new MainLoop();
    StateMachine* stateMachine = new StateMachine(mainLoop);
    if (Parameters::getInstance().allParametersInitialized()) {
        mainLoop->initializeMenu();
        while (!mainLoop->isClosed()) {
            stateMachine->pollEvent();
        }
    }
    delete mainLoop;
    delete stateMachine;
    return 0;
}