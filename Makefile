MAKEFLAGS += -r
CXX=g++
CXXFLAGS=-Wall -pedantic -g -std=c++11 -Wno-long-long -O0 -ggdb -D__RELEASE__
LDLIBS=-pthread -D_REENTRANT -lSDL2 -lSDL2_image -lSDL2_ttf -lm
SRC_DIR=./src/sources
HEADERS_DIR=./src/headers
SRCS=$(wildcard $(SRC_DIR)/*.cpp)
OBJS=$(SRCS:.cpp=.o)

all: compile doc

compile: ./src/main.cpp $(OBJS)
	$(CXX) $(CXXFLAGS) $(LDLIBS) -o karloden $^

$(SRC_DIR)/%.o: $(SRC_DIR)/%.cpp $(HEADERS_DIR)/%.h
	$(CXX) $(CXXFLAGS) $(LDLIBS) -c -o $@ $<

doc:
	doxygen Doxyfile

run:
	./karloden

clean:
	$(RM) -r $(SRC_DIR)/*.o ./doc ./karloden
